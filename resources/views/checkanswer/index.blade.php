@extends('layouts.master')

@section('css')
@endsection

@section('title')
    รายการตรวจคำตอบ
@endsection

@section('breadcrumd')
@endsection

@section('actions')
@endsection

@section('content')
<div class="card card-custom gutter-b" id="kt_card_1">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label">ค้นหาข้อมูล</h3>
        </div>
        <div class="card-toolbar">
            <a href="#" class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="Search">
                <i class="ki ki-arrow-down icon-nm"></i>
            </a>
        </div>
    </div>
    <div class="card-body p-0">
        <form class="form">
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label>ปี-เทอมการศึกษา:</label>
                        <div class="input-group">
                            <select name="filter_term" id="filter_term" class="form-control selectpicker" data-live-search="true">
                                    <option value="">--- เลือกปี เทอมการศึกษา ---</option>
                                @foreach($terms as $term)
                                    <option value="{{$term->id}}" {{$term->id==$filter_term?'selected':''}}>{{$term->fullname}}</option>
                                @endforeach
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                    <div class="col-lg-4">
                        <label>วิชาเรียน:</label>
                        <div class="input-group">
                            <select name="filter_subject" id="filter_subject" class="form-control selectpicker" data-live-search="true">
                                    <option value="">--- เลือกวิชาเรียน ---</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                    <div class="col-lg-4">
                        <label>ชั้นเรียน:</label>
                        <div class="input-group">
                            <select name="filter_classes" id="filter_classes" class="form-control selectpicker" data-live-search="true">
                                    <option value="">--- เลือกชั้นเรียน ---</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="text-right">
                    <button class="btn btn-light-primary font-weight-bold mr-2"><i class="fa fa-search"></i> ค้นหา</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="row">
    @foreach($payloads as $payload)
        <div class="col-xl-6">
            <!--begin::Card-->
            <div class="card card-custom gutter-b card-stretch">
                <!--begin::Body-->
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <!--begin::Info-->
                        <div class="d-flex flex-column mr-auto">
                            <div class="d-flex mb-5 align-items-cente">
                                <span class="d-block font-weight-bold mr-5">บทเรียน</span>
                                <div class="d-flex flex-row-fluid align-items-center">
                                    <span class="ml-3 font-weight-bolder">{{ $payload->code }}</span>
                                </div>
                            </div>
                            <div class="d-flex mb-5 align-items-cente">
                                <span class="d-block font-weight-bold mr-5">ชื่อบทเรียน</span>
                                <div class="d-flex flex-row-fluid align-items-center">
                                    <span class="ml-3 font-weight-bolder">{{ $payload->name }}</span>
                                </div>
                            </div>
                            <div class="d-flex mb-5 align-items-cente">
                                <span class="d-block font-weight-bold mr-5">หมวดหมู่วิชา/หลักสูตร</span>
                                <div class="d-flex flex-row-fluid align-items-center">
                                    <span class="ml-3 font-weight-bolder">{{ $payload->subject ? $payload->subject->name : '' }}</span>
                                </div>
                            </div>
                            <div class="d-flex mb-5 align-items-cente">
                                <span class="d-block font-weight-bold mr-5">ตำแหน่ง</span>
                                <div class="d-flex flex-row-fluid align-items-center">
                                    <span class="ml-3 font-weight-bolder">{{ $payload->sort }}</span>
                                </div>
                            </div>
                        </div>
                        <!--end::Info-->
                    </div>
                    <!--end::Info-->
                    
                    <div class="separator separator-solid my-7"></div>

                    <div class="row">
                        <div class="col-lg-12 col-xs-12">
                            <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample3">
                                @foreach($payload->sheetsets() as $sheetset)
                                    @php
                                        $lablesson = $payload->lablesson($sheetset->id);
                                        if ($lablesson) {
                                            if ($lablesson->lab->level_id == "1") {
                                                $background = "bg-light-success";
                                            } else if ($lablesson->lab->level_id == "2") {
                                                $background = "bg-light-dark";
                                            } else if ($lablesson->lab->level_id == "3") {
                                                $background = "bg-light-danger";
                                            } else {
                                                $background = "bg-light-warning";
                                            }
                                        } else {
                                            $background = "bg-light-warning";
                                        }
                                    @endphp
                                    @if($lablesson)
                                        <a href="{{ route('checkanswer.detail') }}?sheetset={{$sheetset->id}}" class="btn btn-light-success btn-block text-dark font-weight-bold mr-2">
                                            {{$sheetset->code}} {{$lablesson ? $lablesson->name : ''}}
                                        </a>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Body-->
            </div>
            <!--end:: Card-->
        </div>
    @endforeach
</div>
@endsection

@section('script')
<script src="{{asset('js/selected.js')}}"></script>
<script>
    // This card is lazy initialized using data-card="true" attribute. You can access to the card object as shown below and override its behavior
    var card = new KTCard('kt_card_1');

    // Toggle event handlers
    card.on('beforeCollapse', function (card) {
        setTimeout(function () {
            // toastr.info('Before collapse event fired!');
        }, 100);
    });

    card.on('afterCollapse', function (card) {
        setTimeout(function () {
            // toastr.warning('Before collapse event fired!');
        }, 2000);
    });

    card.on('beforeExpand', function (card) {
        setTimeout(function () {
            // toastr.info('Before expand event fired!');
        }, 100);
    });

    card.on('afterExpand', function (card) {
        setTimeout(function () {
            // toastr.warning('After expand event fired!');
        }, 2000);
    });

    $(function(){
        $("#filter_term").trigger("change");
    })

    $(document).on('change', '#filter_term', function(){    // 2nd (B)
        $.ajax({
            type:"get",
            url: "{{ route('lesson.getsubject') }}",
            datatype: "Json",
            async: false,
            data: {
                filter_subject: "{{$filter_subject}}",
                term: $("#filter_term").val()
            }, success(response) {
                if (response.row > 0) {
                    html = "<option value=''>---เลือกวิชาเรียน---</option>";
                    for(var i = 0; i < response.row; i++) {
                        if (response.filter_subject==response.subject[i].subject_id) {
                            html += "<option value='"+response.subject[i].subject_id+"' selected>"+response.subject[i].subject.name+"</option>";
                        } else {
                            html += "<option value='"+response.subject[i].subject_id+"'>"+response.subject[i].subject.name+"</option>";
                        }
                    }
                    $("#filter_subject").html(html);
                    $("#filter_subject").selectpicker('refresh');
                }
            }
        })

        $.ajax({
            type:"get",
            url: "{{ route('lesson.getclasses') }}",
            datatype: "Json",
            async: false,
            data: {
                filter_classes: "{{$filter_classes}}",
                term: $("#filter_term").val()
            }, success(response) {
                console.log(response);
                if (response.row > 0) {
                    html = "<option value=''>---เลือกชั้นเรียน---</option>";
                    for(var i = 0; i < response.row; i++) {
                        if (response.filter_classes==response.classes[i].classes_id) {
                            html += "<option value='"+response.classes[i].classes_id+"' selected>"+response.classes[i].classes.name_th+"</option>";
                        } else {
                            html += "<option value='"+response.classes[i].classes_id+"'>"+response.classes[i].classes.name_th+"</option>";
                        }
                    }
                    $("#filter_classes").html(html);
                    $("#filter_classes").selectpicker('refresh');
                }
            }
        })
    });
</script>
@endsection
