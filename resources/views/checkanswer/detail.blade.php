@extends('layouts.master')

@section('css')
@endsection

@section('title')
    รายการตรวจคำตอบ
@endsection

@section('breadcrumd')
<a href="{{ URL::previous() }}" class="text-primary">รายการข้อมูลตรวจคำตอบ</a>
@endsection

@section('actions')
@endsection

@section('content')
@if($payload->sheetdocuments->count() > 0)
    <div class="card card-custom gutter-b">
        <div class="card-header card-header-tabs-line">
            <div class="card-toolbar">
                <ul class="nav nav-tabs nav-bold nav-tabs-line">
                    @for($numberPages = 0; $numberPages<=$payload->sheetdocuments->count(); $numberPages++)
                        @if($numberPages==0)
                            <li class="nav-item">
                                <a class="nav-link {{ ($numberPages == 0 ? 'active' : '') }}" data-toggle="tab" href="#tab-{{$numberPages}}">
                                    <span class="nav-text">หน้าปก</span>
                                </a>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab-{{$numberPages}}">
                                    <span class="nav-text">หน้าที่ {{ $numberPages }}</span>
                                </a>
                            </li>
                        @endif
                    @endfor
                </ul>
            </div>
        </div>
        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="tab-0" role="tabpanel" aria-labelledby="tab-0">
                    @if($payload->sheetcover)
                        <a class="popup-image" href="{{ (!is_null($payload->sheetcover->cover) ? asset('cover/sheetset/'.$payload->sheetcover->cover) : asset('cover/sheetset/placeholder.png')) }}">
                            <img id="blah" src="{{ (!is_null($payload->sheetcover->cover) ? asset('cover/sheetset/'.$payload->sheetcover->cover) : asset('cover/sheetset/placeholder.png')) }}" 
                                alt="your image" 
                                style="width: 100%;"
                                class="message-avatar">
                        </a>
                    @else
                        <center>
                            ไม่พบภาพหน้าปก
                        </center>
                    @endif
                </div>
                @foreach($payload->sheetdocuments()->orderBy('page', 'asc')->get() as $indexSheet => $document)
                    <div class="tab-pane fade" id="tab-{{++$indexSheet}}" role="tabpanel" aria-labelledby="tab-{{++$indexSheet}}">
                        <h4 class="m-b-lg">
                            รหัสชีท : {{ $document->sheet->code }} {{ $document->sheet->name }}
                        </h4>
                        <div class="row">
                            <div class="col-lg-6">
                                <a class="popup-image" href="{{ (!is_null($document->sheet->image) ? asset('sheets/'.$document->sheet->image) : asset('sheets/placeholder.png')) }}">
                                    <img id="blah" src="{{ (!is_null($document->sheet->image) ? asset('sheets/'.$document->sheet->image) : asset('sheets/placeholder.png')) }}" 
                                        alt="your image" 
                                        style="height: auto; width: 100%;"
                                        class="message-avatar">
                                </a>
                            </div>
                            <div class="col-lg-6">
								@if($document->sheet->sheetquestions->count() > 0)
                                    @foreach($document->sheet->sheetquestions as $indexQuestions => $valueQuestion)
                                        <?php $decodeQuestions = json_decode($valueQuestion->posts); ?>
                                        <div class="well">
                                            <h4 class="m-b">{{ $valueQuestion->title }}</h4>
                                            <div>
                                            @if(!empty($decodeQuestions))
                                                @foreach($decodeQuestions as $indexDecode => $decode)
                                                    @php ++$indexDecode @endphp
                                                    {!! count($decodeQuestions) > 1 ? '<label>ข้อย่อยที่ '.$indexDecode.'.</label>' : null !!}
                                                    @if($decode->type == 'choice')
                                                        @include('checkanswer.template.choice', ["count" => count($decodeQuestions)])
                                                    @endif

                                                    @if($decode->type == 'write')
                                                        @include('checkanswer.template.write', ["decode_function" => empty($decode->function)?0:$decode->function, "count" => count($decodeQuestions) ])
                                                    @endif

                                                    @if($decode->type == 'choicewrite')
                                                        @include('checkanswer.template.choicewrite', ["decode_function" => empty($decode->function)?0:$decode->function, "count" => count($decodeQuestions) ])
                                                    @endif

                                                    @if($decode->type == 'choiceimage')
                                                        @include('checkanswer.template.choiceimage', ["count" => count($decodeQuestions)])
                                                    @endif

                                                    <hr />
                                                @endforeach
                                                
                                                <button class="btn btn-success font-bold m-b m-r-sm"  onclick="answerRespone({{ $valueQuestion->id }}, this)">เฉลย</button>
                                                <div class="v-contents"></div>
                                            @else
                                                <div class="alert alert-warning m-b-none">ยังไม่การกำหนดคำถาม</div>
                                            @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="alert alert-warning m-b-none">ยังไม่การกำหนดคำถาม</div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@else
    <div class="alert alert-warning">ไม่มีข้อมูลสำหรับทำรายการในส่วนนี้</div>
@endif
@endsection

@section('script')
<script>
    function checkingQuestion(element) {
		var refer = $(element).closest('.checkanswer-box');
		var type = refer.attr('question-type');
		var check_number = $(element).attr("data-check");
		check_number = parseInt(check_number) + 1;
		$(element).attr("data-check" , check_number);
		var dataGet = {
			id : refer.attr('question-id'),
			index : refer.attr('question-index'),
			check_number : check_number
		};
		if( check_number <= 2 ) {
			switch(type) {
				case 'choice':
					dataGet.choose = refer.find('.selectpicker option:selected').val();
				break;
				
				case 'write':
					dataGet.choose = refer.find('input[type="text"]').val();
				break;
				
				case 'choiceimage':
					dataGet.choose = refer.find('input[type="radio"]:checked').val();
				break;
				
				case 'choicewrite':
					dataGet.chooseChoice = refer.find('.selectpicker option:selected').val();
					dataGet.chooseWrited = refer.find('input[type="text"]').val();
				break;
			}
			
			$.get('{{ route("checkanswer.check") }}', dataGet, function(response) {
				refer.find('.result-wrong').hide();
				refer.find('.result-cheap').hide();
				
				if (response.result == true) {
					refer.find('.result-cheap').fadeIn();
					refer.find('.selectpicker').prop('disabled', true);
					refer.find('input[type="text"]').prop('readonly', true);
					$(element).prop("disabled" , true);
				} else {
					refer.find('.result-wrong').fadeIn()
					refer.find('input[type="text"]').val(null);
					/* เมื่อเลือกผิดให้ระบบคืนค่าตัวเลือกปัจจุบันออกทันที */
					if (type == 'choice' || type == 'choicewrite') {
						refer.find('.selectpicker').val(0);
						refer.find('.selectpicker').selectpicker('refresh');
						if( check_number == 2 ){
							refer.find('input[type="text"]').prop('readonly', true);
							refer.find('.selectpicker').prop('disabled', true);
						}
					}
					
					$(element).text("ตรวจคำตอบ ("+ check_number +"/2)");
					if( check_number == 2 ){
						$(element).prop("disabled", true);
						// $(element).remove();
					}
				}
			});
		} else {
			console.log("ไม่สามารถตรวจคำตอบนี้ได้แล้ว");
		}
	}
	
	$(document).on('click', '.removeAnswer', function () {
		var element = $(this).closest('.v-contents').parent().find('.v-contents').removeClass('well');
			element.html(null);
	});

	function answerRespone ($question_id, el) {
		$.get('{{ route("checkanswer.getanswer") }}?question='+$question_id+"&type=1").then(function (response) {
			var element = $(el).closest('div').find('.v-contents').addClass('well p-xxs');
			element.html(response.html);
		});
	}

	function answerResponepart ($question_id, el) {
		var refer = $(el).closest('.checkanswer-box');
		var index = refer.attr('question-index');
		$.get('{{ route("checkanswer.getanswer") }}?question='+$question_id+"&type=1&index="+index).then(function (response) {
			var element = $(el).closest('div').find('.v-contents-part').addClass('well p-xxs');
			element.html(response.html);
		});
	}

	$(document).on('click', '.removeAnswerPart', function () {
		var element = $(this).closest('.v-contents-part').parent().find('.v-contents-part').removeClass('well');
			element.html(null);
	});

	$(document).on("keypress keyup blur" , ".numeric" ,function (event) {
	    // $(this).val($(this).val().replace(/[^0-9\.]/g,''));
	    if ( (event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && (event.which!=44) ) {
	        event.preventDefault();
	    }
	});

	$(document).on("keypress keyup blur" , ".numericthai" ,function (event) {
	    // $(this).val($(this).val().replace(/[^0-9\.]/g,''));
	    if ( ( event.which < 3664 || event.which > 3673 ) && (event.which==44) ) {
	        event.preventDefault();
	    }
	});

	$(document).on("keypress keyup blur" , ".english" ,function (event) {
	    if ( (event.which < 65 || event.which > 90) && (event.which < 97 || event.which > 122) ) {
	        event.preventDefault();
	    }
	});

	$(document).on("keypress keyup blur" , ".thailand" ,function (event) {
	    if ( (event.which < 3585 || event.which > 3642) && (event.which < 3647 || event.which > 3675) ) {
	        event.preventDefault();
	    }
	});

	$(document).on("keypress keyup blur" , ".numeric_english" ,function (event) {
	    if ( (event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && ( event.which < 3664 || event.which > 3673 ) && (event.which < 65 || event.which > 90) && (event.which < 97 || event.which > 122) ) {
	        event.preventDefault();
	    }
	});

	$(document).on("keypress keyup blur" , ".numeric_thailand" ,function (event) {
	    if ( (event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57) && (event.which < 3585 || event.which > 3642) && (event.which < 3647 || event.which > 3675) ) {
	        event.preventDefault();
	    }
	});

	$(document).on("keypress keyup blur" , ".english_thailand" ,function (event) {
	    if ( (event.which < 65 || event.which > 90) && (event.which < 97 || event.which > 122) && (event.which < 3585 || event.which > 3642) && (event.which < 3647 || event.which > 3675) ) {
	        event.preventDefault();
	    }
	});
</script>
@endsection
