<div class="checkanswer-box" question-id="{{ $valueQuestion->id }}" question-index="{{ $indexDecode }}" question-type="choicewrite">
	<div class="row">
		<div class="col-sm-8">
			<div class="form-group m-b">
				{{ Form::text('write', null, ['class' => 'form-control', 'placeholder' => 'ระบุบคำตอบในช่องว่างก่อนตรวจคำตอบ']) }}
			</div>
		</div>
		
		<div class="col-sm-4">
			<?php
				$defaultOption 	= ['0' => '--- เลือกรายการ ---'];
				$valueOption	= explode('|', $decode->answerChoice);
				$options		= array_merge($defaultOption, $valueOption);
			?>

			<div class="checkanswer-box">
				<div class="form-group m-b">
					{{ Form::select('', $options, null, ['class' => 'form-control bootstrap-select']) }}
				</div>
			</div>
		</div>
	</div>
	
	{{ Form::button('ตรวจคำตอบ (0/2)', ['class' => 'btn btn-w-m btn-info font-bold m-b m-r-sm', 'data-check' => 0 , 'onclick' => 'checkingQuestion(this);']) }}

	@if($count>1)
		{{ Form::button('เฉลย', ['class' => 'btn btn-w-m btn-info font-bold m-b m-r-sm', 'onclick' => 'answerResponepart( '.$valueQuestion->id.', this)']) }}
	@endif
    
	<i class="fa result-wrong fa-times-circle text-danger fa-2x" style="display: none;"></i>
	<i class="fa result-cheap fa-check-circle text-navy fa-2x" style="display: none;"></i>
    
    <div class="v-contents-part"></div>

	
</div>
