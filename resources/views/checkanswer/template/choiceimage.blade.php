<?php
	$defaultOption 	= ['0' => 'กรุณาเลือกรายการสำหรับตรวจเช็คข้อมูล'];
	$valueOption	= explode('|', $decode->answer);
	$options		= array_merge($defaultOption, $valueOption);
?>

<div class="checkanswer-box" question-id="{{ $valueQuestion->id }}" question-index="{{ $indexDecode }}" question-type="choiceimage">
	<div class="form-group m-b">
		<div class="row">
			@foreach($valueOption as $index => $value)
				<div class="col-sm-12 m-b">
					<label class="m-t font-bold">
						<img src="{{ asset('imagesquestion/'.$value) }}" width="100%" height="150px;">
						{{ Form::radio('images', ++$index) }} เลือกรูปภาพ
					</label>
				</div>
			@endforeach
		</div>
	</div>

	{{ Form::button('ตรวจคำตอบ', ['class' => 'btn btn-w-m btn-info font-bold m-b m-r-sm', 'onclick' => 'checkingQuestion(this);']) }}

	@if($count>1)
		{{ Form::button('เฉลย', ['class' => 'btn btn-w-m btn-info font-bold m-b m-r-sm', 'onclick' => 'answerResponepart( '.$valueQuestion->id.', this)']) }}
	@endif
    
	<i class="fa result-wrong fa-times-circle text-danger fa-2x" style="display: none;"></i>
	<i class="fa result-cheap fa-check-circle text-navy fa-2x" style="display: none;"></i>
    
    <div class="v-contents-part"></div>

	
</div>