<?php
	$defaultOption 	= ['0' => '--- เลือกคำตอบ ---'];
	$valueOption	= explode('|', $decode->answer);
	$options		= array_merge($defaultOption, $valueOption);
?>

<div class="checkanswer-box" question-id="{{ $valueQuestion->id }}" question-index="{{ $indexDecode }}" question-type="choice">
	<div class="form-group m-b">
		{{ Form::select('', $options, null, ['class' => 'form-control selectpicker']) }}
	</div>

	{{ Form::button('ตรวจคำตอบ (0/2)', ['class' => 'btn btn-w-m btn-info font-bold m-b m-r-sm', 'data-check' => 0 , 'onclick' => 'checkingQuestion(this);']) }}

	@if($count>1)
		{{ Form::button('เฉลย', ['class' => 'btn btn-w-m btn-default text-dark font-bold m-b m-r-sm', 'onclick' => 'answerResponepart( '.$valueQuestion->id.', this)']) }}
	@endif
    
	<i class="fa result-wrong fa-times-circle text-danger fa-2x" style="display: none;"></i>
	<i class="fa result-cheap fa-check-circle text-navy fa-2x" style="display: none;"></i>
    
    <div class="v-contents-part"></div>
</div>
