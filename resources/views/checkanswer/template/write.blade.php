<div class="checkanswer-box" question-id="{{ $valueQuestion->id }}" question-index="{{ $indexDecode }}" question-type="write">
	@php($function="")
	@if($decode_function==2)
		@php($function = "numeric")
	@elseif($decode_function==3)
		@php($function = "english")
	@elseif($decode_function==4)
		@php($function = "thailand")
	@elseif($decode_function==5)
		@php($function = "numeric_english")
	@elseif($decode_function==6)
		@php($function = "numeric_thailand")
	@elseif($decode_function==7)
		@php($function = "english_thailand")
	@elseif($decode_function==8)
		@php($function = "numericthai")
	@endif
	
	<div class="form-group m-b">
		{{ Form::text('write', null, ['class' => 'form-control '.$function , 'placeholder' => 'พิมพ์คำตอบที่ถูกต้อง']) }}
	</div>

	{{ Form::button('ตรวจคำตอบ (0/2)', ['class' => 'btn btn-w-m btn-info font-bold m-b m-r-sm' , 'data-check' => 0 , 'onclick' => 'checkingQuestion(this);']) }}

	@if($count>1)
		{{ Form::button('เฉลย', ['class' => 'btn btn-w-m btn-info font-bold m-b m-r-sm', 'onclick' => 'answerResponepart( '.$valueQuestion->id.', this)']) }}
	@endif
    
	<i class="fa result-wrong fa-times-circle text-danger fa-2x" style="display: none;"></i>
	<i class="fa result-cheap fa-check-circle text-navy fa-2x" style="display: none;"></i>

    <div class="v-contents-part"></div>

	
</div>
