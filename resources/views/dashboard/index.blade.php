@extends('layouts.master')

@section('css')
@endsection

@section('title')
    แดชบอร์ด
@endsection

@section('breadcrumd')
@endsection

@section('actions')
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6 col-xxl-4">
            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-label">
                            จำนวนนักเรียน
                        </h3>
                    </div>
                </div>
                <div class="card-body">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled. Lorem Ipsum
                    is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled.
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
@endsection
