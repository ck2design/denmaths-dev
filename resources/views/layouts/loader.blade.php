<div class="page-loader page-loader-logo">
    <img alt="Logo" class="max-h-200px" src="{{ asset('media/logos/logo.png') }}" />
    <div class="spinner spinner-primary"></div>
</div>