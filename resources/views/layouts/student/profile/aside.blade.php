<div class="flex-row-auto offcanvas-mobile w-350px w-xxl-350px" id="kt_profile_aside">
    <!--begin::Profile Card-->
    <div class="card card-custom card-stretch">
        <!--begin::Body-->
        <div class="card-body pt-4">
            <!--begin::User-->
            <div class="d-flex align-items-center">
                <div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
                    @if($payload->Std_image!="")
                        @php( $image = asset($payload->Std_image) )
                    @else
                        @php( $image = asset('media/users/blank.png') )
                    @endif
                    <div class="symbol-label" style="background-image:url({{$image}})"></div>
                    <i class="symbol-badge bg-success"></i>
                </div>
                <div>
                    <a href="#" class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary">
                        {{ $payload->Std_Fname }} {{ $payload->Std_Lname }}
                    </a>
                    <div class="mt-2">
                        @if($payload->Std_Status=="C")
                            <span class="text-success font-weight-bold">กำลังเรียน</span>
                        @elseif($payload->Std_Status=="U")
                            <span class="text-warning font-weight-bold">เลื่อนชั้นเรียน</span>
                        @elseif($payload->Std_Status=="G")
                            <span class="text-primary font-weight-bold">จบการศึกษา</span>
                        @elseif($payload->Std_Status=="R")
                            <span class="text-danger font-weight-bold">ลาออก</span>
                        @endif
                    </div>
                </div>
            </div>
            <!--end::User-->

            <!--begin::Contact-->
            <div class="py-9">
                <div class="d-flex align-items-center justify-content-between mb-2">
                    <span class="text-dark font-weight-bold mr-lg-12 mr-5 mb-lg-0 mb-2">
                        <span class="svg-icon svg-icon-md svg-icon-primary svg-icon-gray-500 mr-1">
                            <!--begin::Svg Icon -->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <mask fill="white">
                                        <use xlink:href="#path-1"></use>
                                    </mask>
                                    <g></g>
                                    <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000"></path>
                                </g>
                            </svg>
                            <!--end::Svg Icon-->
                        </span>
                        {{ $payload->Std_Id }}
                    </span>
                </div>
                <div class="d-flex align-items-center justify-content-between mb-2">
                    <span class="text-dark font-weight-bold mr-lg-12 mr-5 mb-lg-0 mb-2">
                        <span class="svg-icon svg-icon-md svg-icon-primary svg-icon-gray-500 mr-1">
                            <!--begin::Svg Icon -->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <polygon fill="#000000" opacity="0.3" points="5 7 5 15 19 15 19 7"/>
                                    <path d="M11,19 L11,16 C11,15.4477153 11.4477153,15 12,15 C12.5522847,15 13,15.4477153 13,16 L13,19 L14.5,19 C14.7761424,19 15,19.2238576 15,19.5 C15,19.7761424 14.7761424,20 14.5,20 L9.5,20 C9.22385763,20 9,19.7761424 9,19.5 C9,19.2238576 9.22385763,19 9.5,19 L11,19 Z" fill="#000000" opacity="0.3"/>
                                    <path d="M5,7 L5,15 L19,15 L19,7 L5,7 Z M5.25,5 L18.75,5 C19.9926407,5 21,5.8954305 21,7 L21,15 C21,16.1045695 19.9926407,17 18.75,17 L5.25,17 C4.00735931,17 3,16.1045695 3,15 L3,7 C3,5.8954305 4.00735931,5 5.25,5 Z" fill="#000000" fill-rule="nonzero"/>
                                </g>
                            </svg>
                            <!--end::Svg Icon-->
                        </span>
    
                        @php( $term = $payload->term )
                        {{-- {{dd($term)}} --}}
                        @if ($term)
                            ปี {{ $term->year }} เทอม {{ $term->termnumber }}
                        @endif
                    </span>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <span class="text-dark font-weight-bold mr-lg-12 mr-5 mb-lg-0 mb-2">
                        <span class="svg-icon svg-icon-md svg-icon-primary svg-icon-gray-500 mr-1">
                            <!--begin::Svg Icon -->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000"></path>
                                </g>
                            </svg>
                            <!--end::Svg Icon-->
                        </span>
                        {{ $payload->classes->name_th }} ห้อง {{$payload->SubClass}}
                    </span>
                </div>
            </div>
            <!--end::Contact-->

            <!--begin::Nav-->
            <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                <div class="navi-item mb-2">
                    <a href="{{ route('student.profile.subject.index', ['id' => $payload->id]) }}" class="navi-link py-4">
                        <span class="navi-icon mr-2">
                            <i class="ki-md text-dark-50 ki ki-bold-sort"></i>
                        </span>
                        <span class="navi-text font-size-lg">
                            ข้อมูลการเรียนตามรายวิชา
                        </span>
                    </a>
                </div>
                <div class="navi-item mb-2">
                    <a href="{{ route('student.profile.general.index', ['id' => $payload->id]) }}" class="navi-link py-4">
                        <span class="navi-icon mr-2">
                            <i class="icon-md text-dark-50 flaticon-user"></i>
                        </span>
                        <span class="navi-text font-size-lg">
                            ข้อมูลส่วนตัวนักเรียน
                        </span>
                    </a>
                </div>
                <div class="navi-item mb-2">
                    <a href="{{ route('student.profile.growth.index', ['id' => $payload->id]) }}" class="navi-link py-4">
                        <span class="navi-icon mr-2">
                            <i class="icon-md text-dark-50 flaticon-user"></i>
                        </span>
                        <span class="navi-text font-size-lg">
                            ข้อมูลพัฒนาการ
                        </span>
                    </a>
                </div>
                <div class="navi-item mb-2">
                    <a href="{{ route('student.profile.emergency.index', ['id' => $payload->id]) }}" class="navi-link py-4">
                        <span class="navi-icon mr-2">
                            <i class="icon-md text-dark-50 flaticon2-hospital"></i>   
                        </span>
                        <span class="navi-text font-size-lg">
                            ข้อมูลติดต่อฉุกเฉิน
                        </span>
                    </a>
                </div>
                <div class="navi-item mb-2">
                    <a href="{{ route('student.profile.address.index', ['id' => $payload->id]) }}" class="navi-link py-4">
                        <span class="navi-icon mr-2">
                            <i class="icon-md text-dark-50 flaticon-pin"></i>   
                        </span>
                        <span class="navi-text font-size-lg">
                            ข้อมูลที่อยู่
                        </span>
                    </a>
                </div>
                <div class="navi-item mb-2">
                    <a href="{{ route('student.profile.password.index', ['id' => $payload->id]) }}" class="navi-link py-4">
                        <span class="navi-icon mr-2">
                            <i class="icon-md text-dark-50 flaticon-safe-shield-protection"></i>   
                        </span>
                        <span class="navi-text font-size-lg">
                            เปลี่ยนรหัสผ่าน
                        </span>
                    </a>
                </div>               
            </div>
            <!--end::Nav-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Profile Card-->
</div>