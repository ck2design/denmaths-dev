@php( $auth = Auth::user() )

<div class="flex-row-auto offcanvas-mobile w-250px w-xxl-350px" id="kt_profile_aside">
    <!--begin::Profile Card-->
    <div class="card card-custom card-stretch">
        <!--begin::Body-->
        <div class="card-body pt-4">
            <!--begin::User-->
            <div class="d-flex align-items-center">
                <div class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
                    @if($auth->teacher->picture!="")
                        @php( $image = asset($auth->teacher->picture) )
                    @else
                        @php( $image = asset('media/users/blank.png') )
                    @endif
                    <div class="symbol-label" style="background-image:url({{$image}})"></div>
                    <i class="symbol-badge bg-success"></i>
                </div>
                <div>
                    <a href="#" class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary">
                        {{ $auth->teacher->firstname }} {{ $auth->teacher->lastname }}
                    </a>
                    <div class="text-muted">
                        {{ $auth->teacher->position }}
                    </div>
                    <div class="mt-2">
                    </div>
                </div>
            </div>
            <!--end::User-->

            <!--begin::Contact-->
            <div class="py-9">
                <div class="d-flex align-items-center justify-content-between mb-2">
                    <span class="font-weight-bold mr-2">อีเมล:</span>
                    <span class="text-dark font-weight-bold">{{ $auth->teacher->email }}</span>
                </div>
                <div class="d-flex align-items-center justify-content-between mb-2">
                    <span class="font-weight-bold mr-2">เบอร์โทรศัพท์:</span>
                    <span class="text-dark font-weight-bold">{{ $auth->teacher->phone }}</span>
                </div>
                <div class="d-flex align-items-center justify-content-between">
                    <span class="font-weight-bold mr-2">โรงเรียน:</span>
                    <span class="text-dark font-weight-bold">{{ $auth->teacher->school->school_name_th }}</span>
                </div>
            </div>
            <!--end::Contact-->

            <!--begin::Nav-->
            <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                <div class="navi-item mb-2">
                    <a href="{{ route('profile.general.index') }}" class="navi-link py-4">
                        <span class="navi-icon mr-2">
                            <i class="icon-md text-dark-50 flaticon-user"></i>
                        </span>
                        </span>
                        <span class="navi-text font-size-lg">
                            แก้ไขข้อมูลส่วนตัว
                        </span>
                    </a>
                </div>
                <div class="navi-item mb-2">
                    <a href="{{ route('profile.emergency.index') }}" class="navi-link py-4">
                        <span class="navi-icon mr-2">
                            <i class="icon-md text-dark-50 flaticon2-hospital"></i>   
                        </span>
                        <span class="navi-text font-size-lg">
                            แก้ไขข้อมูลติดต่อฉุกเฉิน
                        </span>
                    </a>
                </div>
                <div class="navi-item mb-2">
                    <a href="{{ route('profile.password.index') }}" class="navi-link py-4">
                        <span class="navi-icon mr-2">
                            <i class="icon-md text-dark-50 flaticon-safe-shield-protection"></i>   
                        </span>
                        <span class="navi-text font-size-lg">
                            เปลี่ยนรหัสผ่าน
                        </span>
                    </a>
                </div>
            </div>
            <!--end::Nav-->
        </div>
        <!--end::Body-->
    </div>
    <!--end::Profile Card-->
</div>