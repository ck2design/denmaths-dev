@extends('layouts.master')

@section('css')
@endsection

@section('title')
    แก้ไขข้อมูลส่วนตัว
@endsection

@section('breadcrumd')
@endsection

@section('actions')
<button type="button" class="btn btn-light-info mr-2" id="form-save"><i class="fa fa-save"></i> บันทึก</button>
@endsection

@section('content')
    <div class="d-flex flex-row">
        @include('layouts.profile.aside')

        <!--begin::Body-->
        <div class="flex-row-fluid ml-lg-8">
            <div class="card card-custom card-stretch">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">แก้ไขข้อมูลส่วนตัว</h3>
                        <span class="text-muted font-weight-bold font-size-sm mt-1">อัพเดตข้อมูลส่วนตัวของคุณ</span>
                    </div>
                    <div class="card-toolbar">
                        
                    </div>
                </div>
                <!--end::Header-->
        
                <!--begin::Form-->
                <form method="POST" action="{{ route('profile.general.store') }}" class="form" id="form-data" enctype="multipart/form-data">
                    @csrf
                    @php( $auth = Auth::user() )
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="row">
                            <label class="col-xl-3"></label>
                            <div class="col-lg-9 col-xl-6">
                                <h5 class="font-weight-bold mb-6">ข้อมูลส่วนตัว</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                            <div class="col-lg-9 col-xl-6">
                                @if($auth->teacher->picture!="")
                                    @php( $image = asset($auth->teacher->picture) )
                                @else
                                    @php( $image = asset('media/users/blank.png') )
                                @endif
                                <div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url({{$image}})">
                                    <div class="image-input-wrapper" style="background-image: url({{$image}})"></div>
        
                                    <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                        <input type="file" name="picture" accept=".png, .jpg, .jpeg"/>
                                        <input type="hidden" name="picture_remove"/>
                                    </label>
        
                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>
        
                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>
                                </div>
                                <span class="form-text text-muted">Allowed file types:  png, jpg, jpeg.</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>ชื่อ<span class="text-danger">*</span>:</label>
                                <input type="text" class="form-control" name="firstname" placeholder="" value="{{ $auth->teacher->firstname }}">
                            <div class="fv-plugins-message-container"></div></div>
                            <div class="col-lg-6">
                                <label>นามสกุล<span class="text-danger">*</span>:</label>
                                <input type="text" class="form-control" name="lastname" placeholder="" value="{{ $auth->teacher->lastname }}">
                            <div class="fv-plugins-message-container"></div></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>ชื่อ (ภาษาอังกฤษ):</label>
                                <input type="text" class="form-control" name="firstname_en" placeholder="" value="{{ $auth->teacher->firstname_en }}">
                            <div class="fv-plugins-message-container"></div></div>
                            <div class="col-lg-6">
                                <label>นามสกุล (ภาษาอังกฤษ):</label>
                                <input type="text" class="form-control" name="lastname_en" placeholder="" value="{{ $auth->teacher->lastname_en }}">
                            <div class="fv-plugins-message-container"></div></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">วันเกิด</label>
                            <div class="col-lg-9 col-xl-9">
                                <input class="form-control form-control-lg form-control-solid" type="date" name="birthday" value="{{ $auth->teacher->birthday }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>ส่วนสูง:</label>
                                <input type="number" class="form-control" name="high" min="0" max="300" placeholder="" value="{{ $auth->teacher->high }}">
                            </div>
                            <div class="col-lg-6">
                                <label>น้ำหนัก:</label>
                                <input type="number" class="form-control" name="weight" min="0" max="350" placeholder="" value="{{ $auth->teacher->weight }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>เชื้อชาติ:</label>
                                <input type="text" class="form-control" name="nationality" placeholder="" value="{{ $auth->teacher->nationality }}">
                            </div>
                            <div class="col-lg-6">
                                <label>สัญชาติ:</label>
                                <input type="text" class="form-control" name="race" placeholder="" value="{{ $auth->teacher->race }}">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-xl-3"></label>
                            <div class="col-lg-9 col-xl-6">
                                <h5 class="font-weight-bold mt-10 mb-6">ข้อมูลติดต่อ</h5>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">เบอร์โทรศัพท์</label>
                            <div class="col-lg-9 col-xl-9">
                                <div class="input-group input-group-lg input-group-solid">
                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                    <input type="text" class="form-control form-control-lg form-control-solid" name="phone" value="{{ $auth->teacher->phone }}" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">อีเมล</label>
                            <div class="col-lg-9 col-xl-9">
                                <div class="input-group input-group-lg input-group-solid">
                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                    <input type="text" class="form-control form-control-lg form-control-solid" name="email" value="{{ $auth->teacher->email }}" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">สถานะการทำงาน</label>
                            <div class="col-lg-9 col-xl-9">
                                <select name="status" id="" class="form-control selectpicker">
                                    <option value="">เลือกสถานะการทำงาน</option>
                                    <option value="R" {{$auth->teacher->status=="R"?'selected':''}}>อยู่ในระบบ</option>
                                    <option value="W" {{$auth->teacher->status=="W"?'selected':''}}>ลาออก</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--end::Body-->
                </form>
                <!--end::Form-->
            </div>
        </div>
        <!--end::Body-->
    </div>

    
@endsection

@section('script')
<script>
    // Class definition
    var KTProfile = function () {
        // Elements
        var avatar;
        var offcanvas;

        // Private functions
        var _initAside = function () {
            // Mobile offcanvas for mobile mode
            offcanvas = new KTOffcanvas('kt_profile_aside', {
                overlay: true,
                baseClass: 'offcanvas-mobile',
                //closeBy: 'kt_user_profile_aside_close',
                toggleBy: 'kt_subheader_mobile_toggle'
            });
        }

        var _initForm = function() {
            avatar = new KTImageInput('kt_profile_avatar');
        }

        return {
            // public functions
            init: function() {
                _initAside();
                _initForm();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTProfile.init();
    });
</script>
<script>
    $("#form-save").click(function(){
        $("#form-data").submit();
    });
</script>
@endsection
