@extends('layouts.master')

@section('css')
@endsection

@section('title')
    เปลี่ยนหัสผ่าน
@endsection

@section('breadcrumd')
@endsection

@section('actions')
<button type="button" class="btn btn-light-info mr-2" id="form-save"><i class="fa fa-save"></i> บันทึก</button>
@endsection

@section('content')
    <div class="d-flex flex-row">
        @include('layouts.profile.aside')

        <!--begin::Body-->
        <div class="flex-row-fluid ml-lg-8">
            <div class="card card-custom card-stretch">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">เปลี่ยนรหัสผ่าน</h3>
                        <span class="text-muted font-weight-bold font-size-sm mt-1">เปลี่ยนแปลงข้อมูลรหัสผ่านของคุณ</span>
                    </div>
                    <div class="card-toolbar">
                        
                    </div>
                </div>
                <!--end::Header-->
        
                <!--begin::Form-->
                <form method="POST" action="{{ route('profile.password.store') }}" class="form" id="form-data" enctype="multipart/form-data">
                    @csrf
                    @php( $auth = Auth::user() )
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label text-alert">รหัสผ่านผัจจุบัน</label>
                            <div class="col-lg-9 col-xl-6">
                                <input type="password" class="form-control form-control-lg form-control-solid mb-2" name="password_current" value="" placeholder="รหัสผ่านผัจจุบัน">
                                <span class="text-sm font-weight-bold">หากลืมรหัสผ่านโปรดติดต่อเจ้าหน้าที่ดูแลระบบ</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label text-alert">รหัสผ่านใหม่</label>
                            <div class="col-lg-9 col-xl-6">
                                <input type="password" class="form-control form-control-lg form-control-solid mb-2" name="password" value="" placeholder="รหัสผ่านใหม่?">
                                <span class="text-sm font-weight-bold"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label text-alert">ยืนยันรหัสผ่าน</label>
                            <div class="col-lg-9 col-xl-6">
                                <input type="password" class="form-control form-control-lg form-control-solid mb-2" name="password_confirmation" value="" placeholder="ยืนยันรหัสผ่าน">
                                <span class="text-sm font-weight-bold"></span>
                            </div>
                        </div>
                    </div>
                    <!--end::Body-->
                </form>
                <!--end::Form-->
            </div>
        </div>
        <!--end::Body-->
    </div>

    
@endsection

@section('script')
<script>
    // Class definition
    var KTProfile = function () {
        // Elements
        var avatar;
        var offcanvas;

        // Private functions
        var _initAside = function () {
            // Mobile offcanvas for mobile mode
            offcanvas = new KTOffcanvas('kt_profile_aside', {
                overlay: true,
                baseClass: 'offcanvas-mobile',
                //closeBy: 'kt_user_profile_aside_close',
                toggleBy: 'kt_subheader_mobile_toggle'
            });
        }

        var _initForm = function() {
            avatar = new KTImageInput('kt_profile_avatar');
        }

        return {
            // public functions
            init: function() {
                _initAside();
                _initForm();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTProfile.init();
    });
</script>
<script>
    $("#form-save").click(function(){
        $("#form-data").submit();
    });
</script>
@endsection
