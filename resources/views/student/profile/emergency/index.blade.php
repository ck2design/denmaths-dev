@extends('layouts.master')

@section('css')
@endsection

@section('title')
    แก้ไขข้อมูลนักเรียนติดต่อฉุกเฉิน
@endsection

@section('breadcrumd')
    <a href="{{ route('student.index') }}" class="text-primary">รายการข้อมูลนักเรียน</a>
@endsection

@section('actions')
    <button type="button" class="btn btn-light-info btn-sm mr-2" id="form-save"><i class="fa fa-save"></i> บันทึก</button>
@endsection

@section('content')
    <div class="d-flex flex-row">
        @include('layouts.student.profile.aside', ['payload', $payload])

        <!--begin::Body-->
        <div class="flex-row-fluid ml-lg-8">
            <div class="card card-custom card-stretch">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">แก้ไขข้อมูลนักเรียนติดต่อฉุกเฉิน</h3>
                        <span class="text-muted font-weight-bold font-size-sm mt-1">อัพเดตข้อมูลติดต่อฉุกเฉินของนักเรียน</span>
                    </div>
                    <div class="card-toolbar">
                        
                    </div>
                </div>
                <!--end::Header-->
        
                <!--begin::Form-->
                <form method="POST" action="{{ route('student.profile.emergency.store') }}" class="form" id="form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{$payload->id}}" />
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>ชื่อ - นามสกุล:</label>
                                <input type="text" class="form-control" name="Ercontract" placeholder="" value="{{ $payload->Ercontract }}">
                            <div class="fv-plugins-message-container"></div></div>
                            <div class="col-lg-6">
                                <label>เบอร์โทรศัพท์:</label>
                                <input type="text" class="form-control" name="ErTel" placeholder="" value="{{ $payload->ErTel }}">
                            <div class="fv-plugins-message-container"></div></div>
                        </div>
                        <div class="form-group">
                            <label>รายละเอียดเพิ่มเติม:</label>
                            <input type="text" class="form-control" name="ErRemark" placeholder="" value="{{ $payload->ErRemark }}">
                            <div class="fv-plugins-message-container"></div>
                        </div>
                    </div>
                    <!--end::Body-->
                </form>
                <!--end::Form-->
            </div>
        </div>
        <!--end::Body-->
    </div>

    
@endsection

@section('script')
<script>
    // Class definition
    var KTProfile = function () {
        // Elements
        var avatar;
        var offcanvas;

        // Private functions
        var _initAside = function () {
            // Mobile offcanvas for mobile mode
            offcanvas = new KTOffcanvas('kt_profile_aside', {
                overlay: true,
                baseClass: 'offcanvas-mobile',
                //closeBy: 'kt_user_profile_aside_close',
                toggleBy: 'kt_subheader_mobile_toggle'
            });
        }

        var _initForm = function() {
            avatar = new KTImageInput('kt_profile_avatar');
        }

        return {
            // public functions
            init: function() {
                _initAside();
                _initForm();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTProfile.init();
    });
</script>
<script>
    $("#form-save").click(function(){
        $("#form-data").submit();
    });
</script>
@endsection
