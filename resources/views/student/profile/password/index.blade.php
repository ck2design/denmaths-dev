@extends('layouts.master')

@section('css')
@endsection

@section('title')
    แก้ไขข้อมูลการเข้าสู่ระบบของนักเรียน
@endsection

@section('breadcrumd')
    <a href="{{ route('student.index') }}" class="text-primary">รายการข้อมูลนักเรียน</a>
@endsection

@section('actions')
    <button type="button" class="btn btn-light-info mr-2" id="form-save"><i class="fa fa-save"></i> บันทึก</button>
@endsection

@section('content')
    <div class="d-flex flex-row">
        @include('layouts.student.profile.aside', ['payload', $payload])

        <!--begin::Body-->
        <div class="flex-row-fluid ml-lg-8">
            <div class="card card-custom card-stretch">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">แก้ไขข้อมูลการเข้าสู่ระบบของนักเรียน</h3>
                        <span class="text-muted font-weight-bold font-size-sm mt-1">อัพเดตข้อมูลการเข้าสู่ระบบของนักเรียน</span>
                    </div>
                    <div class="card-toolbar">
                        
                    </div>
                </div>
                <!--end::Header-->
        
                <!--begin::Form-->
                <form method="POST" action="{{ route('student.profile.password.store') }}" class="form" id="form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{$payload->id}}" />
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label text-alert">รหัสผ่านผัจจุบัน</label>
                            <div class="col-lg-9 col-xl-6">
                                <input type="password" class="form-control form-control-lg form-control-solid mb-2" name="password_current" value="" placeholder="รหัสผ่านผัจจุบัน">
                                <span class="text-sm font-weight-bold"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label text-alert">รหัสผ่านใหม่</label>
                            <div class="col-lg-9 col-xl-6">
                                <input type="password" class="form-control form-control-lg form-control-solid mb-2" name="password" value="" placeholder="รหัสผ่านใหม่?">
                                <span class="text-sm font-weight-bold"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label text-alert">ยืนยันรหัสผ่าน</label>
                            <div class="col-lg-9 col-xl-6">
                                <input type="password" class="form-control form-control-lg form-control-solid mb-2" name="password_confirmation" value="" placeholder="ยืนยันรหัสผ่าน">
                                <span class="text-sm font-weight-bold"></span>
                            </div>
                        </div>

                        <hr />
                        <h4 class="card-title font-weight-bolder text-dark mb-2">ชื่อผู้ใช้ & รหัสผ่าน ปัจจุบัน</h4>
                        <div class="d-flex flex-wrap align-items-center justify-content-between w-100">
                            <!--begin::Info-->
                            <div class="d-flex flex-column align-items-cente py-2 w-75">
                                <!--begin::Title-->
                                <span class="text-dark-75 font-weight-bold font-size-lg mb-1">
                                    ชื่อผู้ใช้งาน
                                </span>
                                <!--end::Title-->
                            </div>
                            <!--end::Info-->
        
                            <!--begin::Label-->
                            <span class="label label-lg label-light-primary label-inline font-weight-bold py-4">{{ $payload->Username }}</span>
                            <!--end::Label-->
                        </div>
        
                        <div class="d-flex flex-wrap align-items-center justify-content-between w-100">
                            <!--begin::Info-->
                            <div class="d-flex flex-column align-items-cente py-2 w-75">
                                <!--begin::Title-->
                                <span class="text-dark-75 font-weight-bold text-hover-primary font-size-lg mb-1">
                                    รหัสผ่าน
                                </span>
                                <!--end::Title-->
                            </div>
                            <!--end::Info-->
        
                            <!--begin::Label-->
                            <span class="label label-lg label-light-primary label-inline font-weight-bold py-4">{{ $payload->Password }}</span>
                            <!--end::Label-->
                        </div>
                    </div>
                    <!--end::Body-->

                    
                </form>
                <!--end::Form-->
            </div>
        </div>
        <!--end::Body-->
    </div>
@endsection

@section('script')
<script>
    // Class definition
    var KTProfile = function () {
        // Elements
        var avatar;
        var offcanvas;

        // Private functions
        var _initAside = function () {
            // Mobile offcanvas for mobile mode
            offcanvas = new KTOffcanvas('kt_profile_aside', {
                overlay: true,
                baseClass: 'offcanvas-mobile',
                //closeBy: 'kt_user_profile_aside_close',
                toggleBy: 'kt_subheader_mobile_toggle'
            });
        }

        var _initForm = function() {
            avatar = new KTImageInput('kt_profile_avatar');
        }

        return {
            // public functions
            init: function() {
                _initAside();
                _initForm();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTProfile.init();
    });
</script>
<script>
    $("#form-save").click(function(){
        $("#form-data").submit();
    });
</script>
@endsection
