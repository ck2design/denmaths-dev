@extends('layouts.master')

@section('css')
@endsection

@section('title')
    ข้อมูลการเรียนตามรายวิชา
@endsection

@section('breadcrumd')
    <a href="{{ route('student.index') }}" class="text-primary">รายการข้อมูลนักเรียน</a>
@endsection

@section('actions')
@endsection

@section('content')
    <div class="d-flex flex-row">
        @include('layouts.student.profile.aside', ['payload', $payload])

        <!--begin::Body-->
        <div class="flex-row-fluid ml-lg-8">
            <div class="card card-custom card-stretch">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">ข้อมูลการเรียนตามหมวดหมู่รายวิชา</h3>
                        <span class="text-muted font-weight-bold font-size-sm mt-1">แสดงข้อมูลการเรียนของนักเรียนตามหมวดหมู่รายวิชา</span>
                    </div>
                    <div class="card-toolbar">
                        
                    </div>
                </div>
                <!--end::Header-->
        
                <!--begin::Form-->
                <div class="card-body">
                    <div class="row justify-content-center">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="pl-0 font-weight-bold text-muted  text-uppercase">หลักสูตร</th>
                                            <th class="text-right font-weight-bold text-muted text-uppercase">เริ่มชั้นเรียน</th>
                                            <th class="text-right font-weight-bold text-muted text-uppercase">วันที่เริ่มเรียน</th>
                                            <th class="text-right pr-0 font-weight-bold text-muted text-uppercase">level</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($payload->subjects as $subject)
                                            <tr class="font-weight-boldest font-size-lg">
                                                <td class="pl-0 pt-7">{{$subject->subject->name}}</td>
                                                <td class="text-right pt-7">{{$subject->classes->name_th}}</td>
                                                <td class="text-right pt-7">{{$subject->begindatethai}}</td>
                                                <td class="text-danger pr-0 pt-7 text-right">Level {{$subject->level}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Form-->
            </div>
        </div>
        <!--end::Body-->
    </div>
@endsection

@section('script')
<script>
    // Class definition
    var KTProfile = function () {
        // Elements
        var avatar;
        var offcanvas;

        // Private functions
        var _initAside = function () {
            // Mobile offcanvas for mobile mode
            offcanvas = new KTOffcanvas('kt_profile_aside', {
                overlay: true,
                baseClass: 'offcanvas-mobile',
                //closeBy: 'kt_user_profile_aside_close',
                toggleBy: 'kt_subheader_mobile_toggle'
            });
        }

        var _initForm = function() {
            avatar = new KTImageInput('kt_profile_avatar');
        }

        return {
            // public functions
            init: function() {
                _initAside();
                _initForm();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTProfile.init();
    });
</script>
<script>
    $("#form-save").click(function(){
        $("#form-data").submit();
    });
</script>
@endsection
