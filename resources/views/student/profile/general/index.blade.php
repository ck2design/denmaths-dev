@extends('layouts.master')

@section('css')
@endsection

@section('title')
    แก้ไขข้อมูลนักเรียน
@endsection

@section('breadcrumd')
    <a href="{{ route('student.index') }}" class="text-primary">รายการข้อมูลนักเรียน</a>
@endsection

@section('actions')
    <button type="button" class="btn btn-light-info mr-2" id="form-save"><i class="fa fa-save"></i> บันทึก</button>
@endsection

@section('content')
    <div class="d-flex flex-row">
        @include('layouts.student.profile.aside', ['payload', $payload])

        <!--begin::Body-->
        <div class="flex-row-fluid ml-lg-8">
            <div class="card card-custom card-stretch">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">แก้ไขข้อมูลนักเรียน</h3>
                        <span class="text-muted font-weight-bold font-size-sm mt-1">อัพเดตข้อมูลส่วนตัวของนักเรียน</span>
                    </div>
                    <div class="card-toolbar">
                        
                    </div>
                </div>
                <!--end::Header-->
        
                <!--begin::Form-->
                <form method="POST" action="{{ route('student.profile.general.store') }}" class="form" id="form-data" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{$payload->id}}" />
                    <!--begin::Body-->
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>เลขประจำตัวนักเรียน<span class="text-danger">*</span>:</label>
                                <input type="text" class="form-control" name="code" placeholder="" value="{{ $payload->Std_Id }}">
                            <div class="fv-plugins-message-container"></div></div>
                            <div class="col-lg-6">
                                <label>เลขที่ประจำห้อง:</label>
                                <input type="number" class="form-control" name="number" placeholder="" value="{{ $payload->Number }}">
                            <div class="fv-plugins-message-container"></div></div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>ชั้นเรียน:</label>
                                <select name="classes" id="" class="form-control selectpicker">
                                    <option value="">---เลือกชั้นเรียน---</option>
                                    @foreach($classes as $class)
                                        <option value="{{$class->id}}" {{$payload->Class_ID==$class->id?"selected":""}}>{{$class->name_th}}</option>
                                    @endforeach
                                </select>
                                <div class="fv-plugins-message-container"></div></div>
                            <div class="col-lg-6">
                                <label>ห้อง:</label>
                                <input type="number" class="form-control" name="room" placeholder="" value="{{ $payload->SubClass }}">
                            <div class="fv-plugins-message-container"></div></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">วันเกิด</label>
                            <div class="col-lg-9 col-xl-9">
                                <input class="form-control form-control-lg form-control-solid" type="date" name="birthday" value="{{ $payload->Std_Bdate }}"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>เริ่มใช้ระบบ ชั้น::</label>
                                <select name="begin_class" id="" class="form-control selectpicker">
                                    <option value="">---เลือกชั้นเรียน---</option>
                                    @foreach($classes as $class)
                                        <option value="{{$class->id}}" {{$payload->BeginClass==$class->id?"selected":""}}>{{$class->name_th}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label>เริ่มใช้ระบบ วันที่:</label>
                                <input type="date" class="form-control" name="begin_date" placeholder="" value="{{ $payload->BeginDate }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>ปีการศึกษา เริ่มใช้ระบบ:</label>
                                <select name="term_start" id="" class="form-control selectpicker">
                                    <option value="">---เลือกเทอมการศึกษา---</option>
                                    @foreach($terms as $term)
                                        <option value="{{$term->id}}" {{$payload->start_system_term_id==$term->id?"selected":""}}>{{$term->fullname}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label>ปีการศึกษา ปัจจุบัน:</label>
                                <select name="term" id="" class="form-control selectpicker">
                                    <option value="">---เลือกเทอมการศึกษา---</option>
                                    @foreach($terms as $term)
                                        <option value="{{$term->id}}" {{$payload->term_id==$term->id?"selected":""}}>{{$term->fullname}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">Avatar</label>
                            <div class="col-lg-9 col-xl-6">
                                @if($payload->Std_image!="")
                                    @php( $image = asset($payload->Std_image) )
                                @else
                                    @php( $image = asset('media/users/blank.png') )
                                @endif
                                <div class="image-input image-input-outline" id="kt_profile_avatar" style="background-image: url({{$image}})">
                                    <div class="image-input-wrapper" style="background-image: url({{$image}})"></div>
        
                                    <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                        <input type="file" name="image" accept=".png, .jpg, .jpeg"/>
                                        <input type="hidden" name="image_remove"/>
                                    </label>
        
                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>
        
                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="remove" data-toggle="tooltip" title="Remove avatar">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>
                                </div>
                                <span class="form-text text-muted">Allowed file types:  png, jpg, jpeg.</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>เพศ:</label>
                                <input type="text" class="form-control" disabled value="{{$payload->Std_Sex}}" />
                            </div>
                            <div class="col-lg-6">
                                <label>คำนำหน้าชื่อ:</label>
                                <input type="text" class="form-control" disabled value="{{$payload->Std_Prefix}}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>ชื่อ:</label>
                                <input type="text" class="form-control" disabled value="{{$payload->Std_Fname}}" />
                            </div>
                            <div class="col-lg-6">
                                <label>นามสกุล:</label>
                                <input type="text" class="form-control" disabled value="{{$payload->Std_Lname}}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>ชื่อ (ภาษาอังกฤษ):</label>
                                <input type="text" class="form-control" disabled value="{{$payload->Std_FnameEn}}" />
                            </div>
                            <div class="col-lg-6">
                                <label>นามสกุล (ภาษาอังกฤษ):</label>
                                <input type="text" class="form-control" disabled value="{{$payload->Std_LnameEn}}" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>ชื่อเล่น:</label>
                                <input type="text" name="sname" class="form-control" value="{{$payload->Std_Sname}}" />
                            </div>
                            <div class="col-lg-6">
                                <label>เลขที่ประจำตัวประชาชน:</label>
                                <input type="text" name="person_id" class="form-control" value="{{$payload->Std_PersonalID}}" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>วันเกิด:</label>
                                <input type="date" name="birthday" class="form-control" value="{{$payload->Std_Bdate}}" />
                            </div>
                            <div class="col-lg-6">
                                <label>ศาสนา:</label>
                                <input type="text" name="religious" class="form-control" value="{{$payload->Std_Religious}}" />
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <label>สัญชาติ:</label>
                                <input type="text" name="nationality" class="form-control" value="{{$payload->StdNationality}}" />
                            </div>
                            <div class="col-lg-6">
                                <label>เชื้อชาติ:</label>
                                <input type="text" name="race" class="form-control" value="{{$payload->StdRace}}" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">เลือกสถานะนักเรียน</label>
                            <div class="col-lg-9 col-xl-9">
                                <select name="status" id="" class="form-control selectpicker">
                                    <option value="">---เลือกสถานะนักเรียน---</option>
                                    <option value="R" {{$payload->Std_Status=="C"?'selected':''}}>กำลังเรียน</option>
                                    <option value="W" {{$payload->Std_Status=="U"?'selected':''}}>เลื่อนชั้นเรียน</option>
                                    <option value="W" {{$payload->Std_Status=="G"?'selected':''}}>จบการศึกษา</option>
                                    <option value="W" {{$payload->Std_Status=="R"?'selected':''}}>ลาออก</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--end::Body-->
                </form>
                <!--end::Form-->
            </div>
        </div>
        <!--end::Body-->
    </div>

    
@endsection

@section('script')
<script>
    // Class definition
    var KTProfile = function () {
        // Elements
        var avatar;
        var offcanvas;

        // Private functions
        var _initAside = function () {
            // Mobile offcanvas for mobile mode
            offcanvas = new KTOffcanvas('kt_profile_aside', {
                overlay: true,
                baseClass: 'offcanvas-mobile',
                //closeBy: 'kt_user_profile_aside_close',
                toggleBy: 'kt_subheader_mobile_toggle'
            });
        }

        var _initForm = function() {
            avatar = new KTImageInput('kt_profile_avatar');
        }

        return {
            // public functions
            init: function() {
                _initAside();
                _initForm();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTProfile.init();
    });
</script>
<script>
    $("#form-save").click(function(){
        $("#form-data").submit();
    });
</script>
@endsection
