@extends('layouts.master')

@section('css')
@endsection

@section('title')
    รายการข้อมูลนักเรียน
@endsection

@section('breadcrumd')
@endsection

@section('actions')
@endsection

@section('content')
<div class="card card-custom card-collapse gutter-b" id="kt_card_1">
    <div class="card-header">
        <div class="card-title">
            <h3 class="card-label">ค้นหาข้อมูล</h3>
        </div>
        <div class="card-toolbar">
            <a href="#" class="btn btn-icon btn-sm btn-hover-light-primary mr-1" data-card-tool="toggle" data-toggle="tooltip" data-placement="top" title="Search">
                <i class="ki ki-arrow-down icon-nm"></i>
            </a>
        </div>
    </div>
    <div class="card-body p-0" style="display: none; overflow: hidden;">
        <form class="form">
            <div class="card-body">
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label>ปี-เทอมการศึกษา:</label>
                        <div class="input-group">
                            <select name="filter_term" class="form-control selectpicker" data-live-search="true">
                                    <option value="">--- ทุกเทอมการศึกษา ---</option>
                                @foreach($terms as $term)
                                    <option value="{{$term->id}}" {{$term->id==$filter_term?'selected':''}}>{{$term->fullname}}</option>
                                @endforeach
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                    <div class="col-lg-4">
                        <label>ชั้นเรียน:</label>
                        <div class="input-group">
                            <select name="filter_classes" class="form-control selectpicker" data-live-search="true">
                                    <option value="">--- ทุกชั้นเรียน ---</option>
                                @foreach($classes as $class)
                                    <option value="{{$class->id}}" {{$class->id==$filter_classes?'selected':''}}>{{$class->name_th}}</option>
                                @endforeach
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                    <div class="col-lg-4">
                        <label>สถานะนักเรียน:</label>
                        <div class="input-group">
                            <select name="filter_status" class="form-control selectpicker" data-live-search="true">
                                <option value="">--- ทุกสถานะนักเรียน ---</option>
                                <option value="C" {{$filter_status=="C"?'selected':''}}>กำลังเรียน</option>
                                <option value="U" {{$filter_status=="U"?'selected':''}}>เลื่อนชั้น</option>
                                <option value="G" {{$filter_status=="G"?'selected':''}}>จบการศึกษา</option>
                                <option value="R" {{$filter_status=="R"?'selected':''}}>ลาออก</option>
                            </select>
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-lg-4">
                        <label>รหัสนักเรียน:</label>
                        <div class="input-group">
                            <input type="text" name="filter_code" class="form-control" value="{{$filter_code}}" />
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                    <div class="col-lg-4">
                        <label>ชื่อนักเรียน:</label>
                        <div class="input-group">
                            <input type="text" name="filter_firstname" class="form-control" value="{{$filter_firstname}}" />
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                    <div class="col-lg-4">
                        <label>นามสกุลนักเรียน:</label>
                        <div class="input-group">
                            <input type="text" name="filter_lastname" class="form-control" value="{{$filter_lastname}}" />
                        </div>
                        <span class="form-text text-muted"></span>
                    </div>
                </div>
                <div class="text-right">
                    <button class="btn btn-light-primary font-weight-bold mr-2"><i class="fa fa-search"></i> ค้นหา</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="row">
    @foreach($payloads as $payload)
        <div class="col-xl-4">
            <!--begin::Card-->
            <div class="card card-custom gutter-b card-stretch">
                <!--begin::Body-->
                <div class="card-body">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center">
                        <!--begin::Pic-->
                        @if($payload->image=="")
                            @php( $image = asset('media/users/blank.png') )
                        @else
                            @php( $image = asset($payload->image) )
                        @endif
                        <a href="{{ $image }}" class="image-link">
                            <div class="flex-shrink-0 mr-4 symbol symbol-60 symbol-circle">
                                <img alt="pic" src="{{ $image }}" />
                            </div>
                        </a>
                        <!--end::Pic-->

                        <!--begin::Info-->
                        <div class="d-flex flex-column mr-auto">
                            <!--begin: Title-->
                            <div class="d-flex flex-column mr-auto">
                                <a href="#" class="text-dark text-hover-primary font-size-h4 font-weight-bolder mb-1">
                                    {{ $payload->Std_Fname }} {{$payload->Std_Lname}}
                                </a>
                                @if($payload->Std_Status=="C")
                                    <span class="text-success font-weight-bold">กำลังเรียน</span>
                                @elseif($payload->Std_Status=="U")
                                    <span class="text-warning font-weight-bold">เลื่อนชั้นเรียน</span>
                                @elseif($payload->Std_Status=="G")
                                    <span class="text-primary font-weight-bold">จบการศึกษา</span>
                                @elseif($payload->Std_Status=="R")
                                    <span class="text-danger font-weight-bold">ลาออก</span>
                                @endif
                            </div>
                            <!--end::Title-->
                        </div>
                        <!--end::Info-->

                        <!--begin::Toolbar-->
                        <div class="card-toolbar mb-7">
                        <div class="dropdown dropdown-inline" data-toggle="tooltip" title="Quick actions" data-placement="left">
                            <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="ki ki-bold-more-hor"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                <!--begin::Navigation-->
                                <ul class="navi navi-hover">
                                    <li class="navi-header pb-1">
                                        <span class="text-primary text-uppercase font-weight-bold font-size-sm">การดำเนินการ :</span>
                                    </li>
                                    <li class="navi-item">
                                        <a href="{{ route('student.profile.subject.index', ['id' => $payload->id]) }}" class="navi-link">
                                            {{-- <span class="navi-icon"><i class="flaticon2-edit"></i></span> --}}
                                            <span class="navi-text">ข้อมูลการเรียน</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="{{ route('student.profile.general.index', ['id' => $payload->id]) }}" class="navi-link">
                                            {{-- <span class="navi-icon"><i class="flaticon2-edit"></i></span> --}}
                                            <span class="navi-text">ข้อมูลนักเรียน</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="{{ route('student.profile.growth.index', ['id' => $payload->id]) }}" class="navi-link">
                                            {{-- <span class="navi-icon"><i class="flaticon2-edit"></i></span> --}}
                                            <span class="navi-text">ข้อมูลพัฒนาการ</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="{{ route('student.profile.emergency.index', ['id' => $payload->id]) }}" class="navi-link">
                                            {{-- <span class="navi-icon"><i class="flaticon2-edit"></i></span> --}}
                                            <span class="navi-text">ข้อมูลติดต่อฉุกเฉิน</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="{{ route('student.profile.address.index', ['id' => $payload->id]) }}" class="navi-link">
                                            {{-- <span class="navi-icon"><i class="flaticon2-edit"></i></span> --}}
                                            <span class="navi-text">ข้อมูลที่อยู่</span>
                                        </a>
                                    </li>
                                    <li class="navi-item">
                                        <a href="{{ route('student.profile.password.index', ['id' => $payload->id]) }}" class="navi-link">
                                            {{-- <span class="navi-icon"><i class="flaticon2-edit"></i></span> --}}
                                            <span class="navi-text">รหัสผ่าน</span>
                                        </a>
                                    </li>
                                </ul>
                                <!--end::Navigation-->
                            </div>
                        </div>
                    </div>
                        <!--end::Toolbar-->
                    </div>
                    <!--end::Info-->
                    
                    <!--begin::Contacts-->
                    <div class="d-flex flex-wrap my-2">
                        <span class="text-dark font-weight-bold mr-lg-12 mr-5 mb-lg-0 mb-2">
                            <span class="svg-icon svg-icon-md svg-icon-primary svg-icon-gray-500 mr-1">
                                <!--begin::Svg Icon -->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <mask fill="white">
                                            <use xlink:href="#path-1"></use>
                                        </mask>
                                        <g></g>
                                        <path d="M7,10 L7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 L17,10 L18,10 C19.1045695,10 20,10.8954305 20,12 L20,18 C20,19.1045695 19.1045695,20 18,20 L6,20 C4.8954305,20 4,19.1045695 4,18 L4,12 C4,10.8954305 4.8954305,10 6,10 L7,10 Z M12,5 C10.3431458,5 9,6.34314575 9,8 L9,10 L15,10 L15,8 C15,6.34314575 13.6568542,5 12,5 Z" fill="#000000"></path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            {{ $payload->Std_Id }}
                        </span>
                    </div>
                    <div class="d-flex flex-wrap my-2">
                        <span class="text-dark font-weight-bold mr-lg-12 mr-5 mb-lg-0 mb-2">
                            <span class="svg-icon svg-icon-md svg-icon-primary svg-icon-gray-500 mr-1">
                                <!--begin::Svg Icon -->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <polygon fill="#000000" opacity="0.3" points="5 7 5 15 19 15 19 7"/>
                                        <path d="M11,19 L11,16 C11,15.4477153 11.4477153,15 12,15 C12.5522847,15 13,15.4477153 13,16 L13,19 L14.5,19 C14.7761424,19 15,19.2238576 15,19.5 C15,19.7761424 14.7761424,20 14.5,20 L9.5,20 C9.22385763,20 9,19.7761424 9,19.5 C9,19.2238576 9.22385763,19 9.5,19 L11,19 Z" fill="#000000" opacity="0.3"/>
                                        <path d="M5,7 L5,15 L19,15 L19,7 L5,7 Z M5.25,5 L18.75,5 C19.9926407,5 21,5.8954305 21,7 L21,15 C21,16.1045695 19.9926407,17 18.75,17 L5.25,17 C4.00735931,17 3,16.1045695 3,15 L3,7 C3,5.8954305 4.00735931,5 5.25,5 Z" fill="#000000" fill-rule="nonzero"/>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
        
                            @php( $term = $payload->term )
                            {{-- {{dd($term)}} --}}
                            @if ($term)
                                ปี {{ $term->year }} เทอม {{ $term->termnumber }}
                            @endif
                        </span>
                    </div>
                    <div class="d-flex flex-wrap my-2">
                        <span class="text-dark font-weight-bold mr-lg-12 mr-5 mb-lg-0 mb-2">
                            <span class="svg-icon svg-icon-md svg-icon-primary svg-icon-gray-500 mr-1">
                                <!--begin::Svg Icon -->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"></rect>
                                        <path d="M9.82829464,16.6565893 C7.02541569,15.7427556 5,13.1079084 5,10 C5,6.13400675 8.13400675,3 12,3 C15.8659932,3 19,6.13400675 19,10 C19,13.1079084 16.9745843,15.7427556 14.1717054,16.6565893 L12,21 L9.82829464,16.6565893 Z M12,12 C13.1045695,12 14,11.1045695 14,10 C14,8.8954305 13.1045695,8 12,8 C10.8954305,8 10,8.8954305 10,10 C10,11.1045695 10.8954305,12 12,12 Z" fill="#000000"></path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            {{ $payload->classes->name_th }} ห้อง {{$payload->SubClass}}
                        </span>
                    </div>
                    <!--end::Contacts-->

                    <!--begin::Description-->
                    <div class="mb-10 mt-5 font-weight-bold ">
                        @if($payload->subjects->last())
                            <span class="text-primary font-weight-bold">{{$payload->subjects->last()->subject->name}}</span>
                            <span class="text-dark font-weight-bold">{{$payload->classes->level_th}}</span>
                        @endif
                    </div>
                    <!--end::Description-->
                </div>
                <!--end::Body-->
            </div>
            <!--end:: Card-->
        </div>
    @endforeach
</div>
<div class="card card-custom">
    <div class="card-body py-7">
        <!--begin::Pagination-->
        <div class="d-flex justify-content-between align-items-center flex-wrap">
            <div class="d-flex flex-wrap mr-3">
                {{-- maximum number of links (a little bit inaccurate, but will be ok for now) --}}
                <?php
                    $link_limit = 10;
                    $count = 1;
                ?>
                @if ($payloads->lastPage() > 1)
                    <a href="{{ $payloads->url(1) }}" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 {{ ($payloads->currentPage() == 1) ? ' disabled' : '' }}"><i class="ki ki-bold-double-arrow-back icon-xs"></i></a>
                    <a href="{{ $payloads->previousPageUrl() }}" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 {{ ($payloads->currentPage() == 1) ? ' disabled' : '' }}"><i class="ki ki-bold-arrow-back icon-xs"></i></a>
                    
                    @for($i=1;$i<=$payloads->lastPage();$i++)
                        <?php
                            $half_total_links = floor($link_limit / 2);
                            $from = $payloads->currentPage() - $half_total_links;
                            $to = $payloads->currentPage() + $half_total_links;
                            if ($payloads->currentPage() < $half_total_links) {
                                $to += $half_total_links - $payloads->currentPage();
                            }

                            if ($payloads->lastPage() - $payloads->currentPage() < $half_total_links) {
                                $from -= $half_total_links - ($payloads->lastPage() - $payloads->currentPage()) - 1;
                            }
                        ?>
                        @if ($from < $i && $i < $to)
                            <a href="{{ $payloads->url($i) }}" class="btn btn-icon btn-sm btn-hover-primary mr-2 my-1 {{($payloads->currentPage()==$i)?'active':''}}">{{$i}}</a>
                        @endif

                    @endfor
                    <a href="{{ $payloads->nextPageUrl() }}" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 {{ ($payloads->currentPage() == $payloads->lastPage()) ? ' disabled' : '' }}"><i class="ki ki-bold-arrow-next icon-xs"></i></a>
                    <a href="{{ $payloads->url($payloads->lastPage()) }}" class="btn btn-icon btn-sm btn-light-primary mr-2 my-1 {{ ($payloads->currentPage() == $payloads->lastPage()) ? ' disabled' : '' }}"><i class="ki ki-bold-double-arrow-next icon-xs"></i></a>
                @endif
            </div>
            <div class="d-flex align-items-center">
                <span class="text-muted">Displaying {{$payloads->firstItem()}} - {{ $payloads->firstItem() + $payloads->count() - 1 }} of {{$payloads->total()}} records</span>
            </div>
        </div>
        <!--end:: Pagination-->
    </div>
</div>
@endsection

@section('script')
<script>
    // This card is lazy initialized using data-card="true" attribute. You can access to the card object as shown below and override its behavior
    var card = new KTCard('kt_card_1');

    // Toggle event handlers
    card.on('beforeCollapse', function (card) {
        setTimeout(function () {
            // toastr.info('Before collapse event fired!');
        }, 100);
    });

    card.on('afterCollapse', function (card) {
        setTimeout(function () {
            // toastr.warning('Before collapse event fired!');
        }, 2000);
    });

    card.on('beforeExpand', function (card) {
        setTimeout(function () {
            // toastr.info('Before expand event fired!');
        }, 100);
    });

    card.on('afterExpand', function (card) {
        setTimeout(function () {
            // toastr.warning('After expand event fired!');
        }, 2000);
    });

    $('.image-link').magnificPopup({
        type:'image'
    });
</script>
@endsection
