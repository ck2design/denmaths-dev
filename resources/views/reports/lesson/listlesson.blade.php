<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <head>
        {{-- <link href="{{ asset('css/bootstrap-pdf.css') }}" rel="stylesheet"> --}}
        <style>
            @font-face {
                font-family: 'THSarabunNew';
                font-style: normal;
                font-weight: normal;
                src: url("{{ asset('fonts/THSarabunNew.ttf') }}") format('truetype');
            }
            @font-face {
                font-family: 'THSarabunNew';
                font-style: normal;
                font-weight: bold;
                src: url("{{ asset('fonts/THSarabunNewBold.ttf') }}") format('truetype');
            }
            @font-face {
                font-family: 'THSarabunNew';
                font-style: italic;
                font-weight: normal;
                src: url("{{ asset('fonts/THSarabunNewItalic.ttf') }}") format('truetype');
            }
            @font-face {
                font-family: 'THSarabunNew';
                font-style: italic;
                font-weight: bold;
                src: url("{{ asset('fonts/THSarabunNewBoldItalic.ttf') }}") format('truetype');
            }
            html {
                -webkit-text-size-adjust: 100%;
                    -ms-text-size-adjust: 100%;
            }
            body {
                margin: 0;
                padding: 0;
                font-family: 'THSarabunNew';
            }
            footer,
            header {
                display: block;
            }
            .page-header {
                border: 1px solid #000;
            }
            .text-center {
                text-align: center;
            }
            table {
                background-color: transparent;
            }
            .table-bordered tbody tr td, .table-bordered thead tr th, .table-bordered tfoot tr th, .table-bordered tr td {
                border: 0.01px solid gray;
            }
            h3,h4 {
                line-height: 1px;
                font-weight: normal
            }
            p {
                line-height: 1px;
            }
            .text-right {
                text-align: right;
            }
        </style>
    </head>
    <body>
        <header class="page-header">
            <div class="text-center" >
                <table width="100%" class="table-bordered" cellspacing="0" cellpadding="0" style="font-size:16pt;">
                    <tr style="border: 0px solid #fff;">
                        <td  class="text-left" style="border: 0px solid #fff;">
                            @if(!empty($school->school_logo))
                                <img src="{{ asset('uploads/school/' . $school->school_logo) }}" style="height: 70px; padding-left: 15px;" />
                            @endif
                        </td>

                        <td class="text-center" style="border: 0px solid #fff;">
                            <h4>รายงานโครงสร้างแผนการเรียน</h4>

                            <h4><b>{{ $school->school_name_th }}</b></h4>
                            <h4>ปีการศึกษา {{ $term->year }} เทอม {{ $term->fullname }} ระดับชั้นเรียน {{ $classes->name_th }} </h4>
                            <h4>ของ {{ $subject->subjectmain->name }}/{{ $subject->name }} </h4>
                            <p style="font-size: 14pt; font-weight: normal;">ฝ่ายวิชาการโรงเรียน: {{ $school->school_name_th }}</p>
                        </td>

                        <td class="text-right" style="border: 0px solid #fff;">
                            <img src="{{ asset('media/logos/logo.png') }}" style="height: 70px;"/>
                        </td>
                    </tr>
                </table>
            </div>
        </header>

        <br />

        <div style="">
            <table width="100%" class="table-bordered" cellspacing="0" cellpadding="0" style="font-size: 14pt">
                <thead>
                    <tr>
                        <th class="text-center" style="vertical-align: middle">ลำดับที่</th>
                        <th class="text-center" style="vertical-align: middle;">รหัสบทเรียน</th>
                        <th class="text-center" style="vertical-align: middle;">ชื่อบทเรียน</th>                                
                        <th class="text-center" style="vertical-align: middle;">รายการแลป</th>                                
                        <th class="text-center" style="vertical-align: middle;">รายการแบบฝึก</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $c = 1; $prevCode = ''; $temp=""; ?>
                    @foreach($payloads as $key => $payload)
                        @foreach($payload->sheetsets() as $sheetset)
                            @php
                                $lablesson = $payload->lablesson($sheetset->id);
                            @endphp
                            @if ($lablesson)
                                <tr>
                                    <td>{{$key + 1}}</td>
                                    <td class="text-center">{{ $payload->code }} </td>
                                    <td style="padding-left:10px;">
                                        {{ $payload->name }} ( <small><i class="fa fa-shield text-info"></i> {{ $lablesson ? $lablesson->lab->level->level : '' }}</small> )
                                    </td>
                                    <td style="padding-left:10px;">
                                        {{ $lablesson ? $lablesson->code : '' }} <small class="text-success">{{ $lablesson ? $lablesson->code : '' }}</small>
                                    </td>
                                    <td style="padding-left:10px;">
                                        {{ $sheetset->code }} <small class="text-navy">{{ $sheetset->name }}</small>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endforeach
                </tbody>
                <tfoot>
                    <div>
                        {{--  --}}
                    </div>
                </tfoot>
            </table>
        </div>

        <footer class="page-footer">
            <div class="text-right" >
                วันที่แก้ไขข้อมูลล่าสุด: {{ date('d/m') . '/' . (date('Y') + 543) . ' ' . date('H:i') }}
            </div>
        </footer>
    </body>
</html>