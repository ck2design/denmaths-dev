<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'Auth\LoginController@index')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');

Route::middleware(['auth'])->group(function () {
	Route::get('logout', 'Auth\LoginController@logout')->name('logout');
	Route::get('dashboard', 'DashboardController@index')->name('dashboard.index');

	Route::prefix('api')->namespace('api')->name('api.')->group(function() {
		Route::prefix('api')->namespace('api')->name('api.')->group(function() {

		});
	});

	Route::prefix('profile')->namespace('profile')->name('profile.')->group(function() {
		Route::resource('general', 'GeneralController');
		Route::resource('emergency', 'EmergencyController');
		Route::resource('password', 'PasswordController');
	});
	Route::resource('profile', 'ProfileController');

	Route::resource('student', 'StudentController', [
		'only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']
	]);
	Route::prefix('student')->namespace('student')->name('student.')->group(function() {
		Route::prefix('profile')->namespace('profile')->name('profile.')->group(function() {
			Route::resource('general', 'GeneralController');
			Route::resource('growth', 'GrowthController');
			Route::resource('emergency', 'EmergencyController');
			Route::resource('address', 'AddressController');
			Route::resource('subject', 'SubjectController');
			Route::resource('password', 'PasswordController');
		});
	});

	Route::resource('lesson', 'LessonController',[
		'only' => ['index']
	]);
	Route::prefix('lesson')->name('lesson.')->group(function() {
		Route::get('getsubject', 'LessonController@getSubject')->name('getsubject');
		Route::get('getclasses', 'LessonController@getClasses')->name('getclasses');
		Route::get('getroom', 'LessonController@getRoom')->name('getroom');
		Route::get('download', 'LessonController@download')->name('download');
	});

	Route::resource('checkanswer', 'CheckAnswerController', [
		'only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']
	]);
	Route::prefix('checkanswer')->name('checkanswer.')->group(function() {
		Route::get('detail', 'CheckAnswerController@detail')->name('detail');
		Route::get('check', 'CheckAnswerController@check')->name('check');
		Route::get('getanswer', 'CheckAnswerController@getAnswer')->name('getanswer');
	});

	Route::resource('checkexercise', 'CheckExerciseController', [
		'only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']
	]);
	Route::prefix('checkexercise')->name('checkexercise.')->group(function() {
		Route::get('approve', 'CheckExerciseController@approve')->name('approve');
	});
});
