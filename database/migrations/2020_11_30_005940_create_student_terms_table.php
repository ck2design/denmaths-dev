<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_student_terms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('student_id')->commnet("รหัสนักเรียน");
            $table->unsignedBigInteger('code')->commnet("เลขที่ประจำตัว");
            $table->unsignedBigInteger('classes_id')->comment("ชั้นเรียน");
            $table->unsignedTinyInteger('room')->comment("ห้องเรียน");
            $table->unsignedTinyInteger('number')->comment("เลขที่ห้องเรียน");
            $table->year('year')->comment("ปีการศึกษา");
            $table->unsignedBigInteger("term_number")->comment("เทอมการศึกษา");
            $table->unsignedBigInteger('term_id')->comment("รหัสปีเทอมการศึกษา");
            $table->unsignedBigInteger("subject_id")->nullable()->default(0)->comment("วิชาเรียน");
            $table->unsignedBigInteger("status")->nullable()->default(0)->comment("1. ปัจจุบัน, 0. อดีต");
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_student_terms');
    }
}
