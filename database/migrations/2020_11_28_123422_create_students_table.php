<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_students', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('school_id')->commnet("รหัสโรงเรียน");
            $table->string('code', 16)->comment("เลขที่ประจำตัว");
            $table->date('date_begin')->nullable()->comment("วันที่เริ่มใช้ระบบ");
            $table->date('birthday')->nullable()->comment("วันเกิด");
            $table->text('comment')->nullable()->comment("แนะนำนักเรียน");
            $table->string('firstname', 50)->nullable()->comment("ชื่อ");
            $table->string('lastname', 50)->nullable()->comment("นามสกุล");
            $table->string('firstname_en', 50)->nullable()->comment("ชื่อ ภาษาอังกฤษ");
            $table->string('lastname_en', 50)->nullable()->comment("นามสกุล ภาษาอังกฤษ");
            $table->string('nickname', 50)->nullable()->comment("ชื่อเล่น");
            $table->string('sex', 20)->nullable()->comment("เพศ");
            $table->unsignedTinyInteger('prefix_id')->nullable()->comment("คำนำหน้า");
            $table->unsignedTinyInteger("weight")->nullalbe()->comment("");
            $table->unsignedTinyInteger("height")->nullalbe()->comment("");
            $table->string("address")->nullable()->comment("");
            $table->unsignedBigInteger("sub_district_id")->nullalbe()->comment("");
            $table->unsignedBigInteger("district_id")->nullalbe()->comment("");
            $table->unsignedBigInteger("province_id")->nullalbe()->comment("");
            $table->unsignedSmallInteger("postal_code")->nullalbe()->comment("");
            $table->string("phone", 20)->nullable()->comment("เบอร์โทร");
            $table->string("mobile", 20)->nullalbe()->comment("");
            $table->string("email", 100)->nullalbe()->comment("");
            $table->string("remark")->nullalbe()->comment("");
            $table->string("image", 100)->nullalbe()->comment("");
            $table->string('nationality')->nullable()->comment("");
            $table->string("race")->nullalbe()->comment("");
            $table->unsignedBigInteger("point")->nullalbe()->default(0)->comment("point เก็บไว้แลกของรางวัล");
            $table->string("parent1_name", 100)->nullable()->comment("");
            $table->string("parent1_phone", 20)->nullable()->comment("");
            $table->string("parent1_relation", 50)->nullable()->comment("");
            $table->string("parent2_name", 100)->nullable()->comment("");
            $table->string("parent2_phone", 20)->nullable()->comment("");
            $table->string("parent2_relation", 50)->nullable()->comment("");
            $table->unsignedBigInteger('status')->comment("สถานะชั้นเรียน");
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_students');
    }
}
