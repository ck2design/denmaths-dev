<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dim_teachers', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->unsignedBigInteger('school_id')->comment("รหัสโรงเรียน");
            $table->string('code_new')->nullable();
            $table->string('code_old')->nullable();
            $table->string('number_r8')->nullable();
            $table->string('firstname')->comment("ชื่อ");
            $table->string('lastname')->commnet("นามสกุล");
            $table->string('nickname')->comment("ชื่อเล่น");
            $table->string('firstname_en')->nullable()->comment("ชื่อภาษาอังกฤษ");
            $table->string('lastname_en')->nullable()->commnet("นามสกุลภาษาอังกฤษ");
            $table->string('position')->nullable()->comment("ตำแหน่ง");
            $table->date('birthday')->nullable()->comment("วันเกิด");
            $table->unsignedTinyInteger('age')->nullable()->comment("อายุ");
            $table->string("domicile")->nullable();
            $table->unsignedTinyInteger("high")->nullable()->comment("ความสูง");
            $table->unsignedTinyInteger("weight")->nullable()->comment("น้ำหนัก");
            $table->string("sex")->nullable()->comment("เพศ");
            $table->string("nationality")->nullable()->comment("เชื้อชาติ");
            $table->string("race")->nullable()->commnet("สัญชาติ");
            $table->string("religion")->nullable()->comment("ศาสนา");
            $table->string("firstname_father")->nullable();
            $table->string("lastname_father")->nullable();
            $table->string("firstname_mother")->nullable();
            $table->string("lastname_mother")->nullable();
            $table->string("firstname_fan")->nullable();
            $table->string("lastname_fan")->nullable();
            $table->string("trade")->nullable();
            $table->string("person_id")->nullable();
            $table->string("card_vat")->nullable();
            $table->string("card_drive")->nullable();
            $table->string("car_regis")->nullable();
            $table->string("vilage")->nullable();
            $table->string("home_number")->nullable();
            $table->string("moo")->nullable();
            $table->string("road")->nullable();
            $table->unsignedBigInteger("district_id")->nullable();
            $table->unsignedBigInteger("sub_district_id")->nullable();
            $table->unsignedBigInteger("province_id")->nullable();
            $table->unsignedSmallInteger("zipcode")->nullable();
            $table->string("phone")->nullable();
            $table->string("vilage_current")->nullable();
            $table->string("home_number_current")->nullable();
            $table->string("moo_current")->nullable();
            $table->string("road_current")->nullable();
            $table->unsignedBigInteger("district_current_id")->nullable();
            $table->unsignedBigInteger("sub_district_current_id")->nullable();
            $table->unsignedBigInteger("province_current_id")->nullable();
            $table->unsignedSmallInteger("zipcode_current")->nullable();
            $table->date('work_start')->nullable()->comment("วันที่เริ่มงาน");
            $table->date('work_in')->nullable()->comment("วันที่เข้างาน");
            $table->string('bank_number')->nullable()->comment("เลขที่บัญชี");
            $table->string('chpk_no')->nullable()->comment("");
            $table->string('chps_no')->nullable()->comment("");
            $table->string('department')->nullable()->comment("ตำแหน่ง");
            $table->string('picture')->nullable()->comment("รูปภาพ");
            $table->unsignedTinyInteger('prefix_id')->nullable()->comment("คำนำหน้าชื่อ");
            $table->text('portfolio')->nullable()->comment("ผลงาน");
            $table->string('emergency_name1')->nullable();
            $table->string('emergency_relationship1')->nullable();
            $table->string('emergency_name2')->nullable();
            $table->string('emergency_relationship2')->nullable();
            $table->string('emergency_name3')->nullable();
            $table->string('emergency_relationship3')->nullable();
            $table->string('status', 1)->nullable()->comment("R = กำลังสอน, W = ลาออก");
            $table->string("email")->nullalbe()->comment("อีเมล");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dim_teachers');
    }
}
