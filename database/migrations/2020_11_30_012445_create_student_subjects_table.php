<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fact_student_subjects', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('student_id')->commnet("รหัสนักเรียน");
            $table->unsignedBigInteger('subject_id')->commnet("หมวดวิชา");
            $table->unsignedBigInteger('classes_id')->commnet("หมวดวิชา");
            $table->datetime('begin')->nullalbe()->commnet("หมวดวิชา");
            $table->unsignedBigInteger('present')->commnet("ชั้นเรียนปัจจุบัน");
            $table->unsignedTinyInteger("level")->comment("ระดับความยาก");
            $table->timestamps();
            $table->softdeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fact_student_subjects');
    }
}
