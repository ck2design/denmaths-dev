<?php

namespace App\Http\Requests\student\profile;

use Alert;
use Illuminate\Foundation\Http\FormRequest;

class GrowthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'weight' => 'nullable|numeric|min:0|max:150',
            'height' => 'nullable|numeric|min:0|max:200',
        ];
    }

    public function withValidator($validator)
    {
        $messages = $validator->messages();
        $txtError = "";
        foreach($messages->all() as $message) {
            $txtError .= $message."<br />";
        }

        Alert::html('ข้อมูลผิดพลาด', $txtError, 'error');

        return $validator->errors()->all();
    }
}
