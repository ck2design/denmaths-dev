<?php

namespace App\Http\Requests\student\profile;

use Alert;
use Illuminate\Foundation\Http\FormRequest;

class GeneralRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required|in:C,U,G,R'
        ];
    }

    public function withValidator($validator)
    {
        $messages = $validator->messages();
        $txtError = "";
        foreach($messages->all() as $message) {
            $txtError .= $message."<br />";
        }

        Alert::html('ข้อมูลผิดพลาด', $txtError, 'error');

        return $validator->errors()->all();
    }
}
