<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Alert;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'username' => 'required|email|string|email:rfc,dns',
            'username' => 'required|string',
            'password' => 'required|string|min:8'
        ];
    }

    public function withValidator($validator)
    {
        $messages = $validator->messages();
        $txtError = "";
        foreach($messages->all() as $message) {
            $txtError .= $message."<br />";
        }

        Alert::html('ข้อมูลผิดพลาด', $txtError, 'error');

        return $validator->errors()->all();
    }
}
