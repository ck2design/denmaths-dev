<?php

namespace App\Http\Requests\profile;

use Alert;
use Illuminate\Foundation\Http\FormRequest;

class GeneralRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'birthday' => 'nullable|date',
            'high' => 'nullable|between:0,300',
            'weight' => 'nullable|between:0,350',
            'email' => 'nullable|email:rfc,dns',
            'phone' => 'nullable|string|max:10',
            'status' => 'required|in:R,W'
        ];
    }

    public function withValidator($validator)
    {
        $messages = $validator->messages();
        $txtError = "";
        foreach($messages->all() as $message) {
            $txtError .= $message."<br />";
        }

        Alert::html('ข้อมูลผิดพลาด', $txtError, 'error');

        return $validator->errors()->all();
    }
}
