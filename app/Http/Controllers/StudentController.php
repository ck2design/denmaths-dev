<?php

namespace App\Http\Controllers;

use Alert;
use App\Models\Classes;
use App\Models\Student;
use App\Models\Term;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter_classes = $request->input('filter_classes') ?? $request->input('filter_classes');
        $filter_code = $request->input('filter_code') ?? $request->input('filter_code');
        $filter_firstname = $request->input('filter_firstname') ?? $request->input('filter_firstname');
        $filter_lastname = $request->input('filter_lastname') ?? $request->input('filter_lastname');
        $filter_status = $request->input('filter_status') ?? $request->input('filter_status');
        $filter_term = $request->input('filter_term') ?? $request->input('filter_term');

        $terms = Term::where('school_id', Auth()->user()->school_id)
            ->orderBy('year', 'desc')
            ->orderBy('term', 'asc')
            ->get();
        
        $classes = Classes::all();
        // dd(Auth()->user()->school_id);
        $payloads = Student::where('SchoolId', Auth()->user()->school_id)
            ->when($filter_term, function($query) use($filter_term) {
                $query->where('term_id', $filter_term);
            })->when($filter_classes, function($query) use($filter_classes) {
                $query->where('Class_ID', $filter_classes);
            })->when($filter_status, function($query) use($filter_status) {
                $query->where('Std_Status', $filter_status);
            })->when($filter_code, function($query) use($filter_code) {
                $query->where('Std_Id', "LIKE", "%".$filter_code."%");
            })->when($filter_firstname, function($query) use($filter_firstname) {
                $query->where('firstname', "LIKE", "%".$filter_firstname."%");
            })->when($filter_lastname, function($query) use($filter_lastname) {
                $query->where('lastname', 'LIKE', "%".$filter_lastname."%");
            })->with('subjects')
            ->with('term')
            ->with('classes')
            ->orderBy('Std_Id', 'asc')
            ->paginate(10);
            // dd($payloads);
        return view('student.index', compact(
            'terms',
            'classes',
            'payloads',
            'filter_term',
            'filter_classes',
            'filter_status',
            'filter_firstname',
            'filter_lastname',
            'filter_code'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
