<?php

namespace App\Http\Controllers;

use Alert;
use App\Models\Advisor;
use App\Models\Classes;
use App\Models\School;
use App\Models\SubjectLesson;
use App\Models\Subject;
use App\Models\Term;
use Illuminate\Http\Request;
use PDF;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter_term = $request->input('filter_term')==""?null:$request->input('filter_term');
        $filter_subject = $request->input('filter_subject')==""?null:$request->input('filter_subject');
        $filter_classes = $request->input('filter_classes')==""?null:$request->input('filter_classes');
        $payloads = array();

        $terms = Term::where('school_id', Auth()->user()->school_id)
            ->orderBy('year', 'desc')
            ->orderBy('term', 'asc')
            ->get();
        
        if ($filter_term != null && $filter_subject != null && $filter_classes != null) {
            $term = Term::where('id', $request->input('filter_term'))
                ->firstorfail();

            $payloads = SubjectLesson::where([
                'subject_lesson.subject_id' => $filter_subject,
                'subject_lesson.classes_id' => $filter_classes,
                'subject_lesson.year'       => $term->year,
            ])->select('subject_lesson.*','subject_lesson_school_order.sort AS school_sort')
            ->leftJoin('subject_lesson_school_order', 'subject_lesson_school_order.lesson_id', '=', 'subject_lesson.id')
            ->where('subject_lesson_school_order.sort', '>', '0')
            ->where('subject_lesson_school_order.school_id', Auth()->user()->school_id)
            ->where('subject_lesson_school_order.term_id', $filter_term)
            ->orderBy('level_id')->orderBy('school_sort')
            ->get();
        }

        return view('lesson.index', compact([
            'terms',
            'filter_term',
            'filter_subject',
            'filter_classes',
            'payloads'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSubject(Request $request)
    {
        $subject = Advisor::where('SchTeacher_id',  Auth()->user()->teacher->id)
            ->where('school_id', Auth()->user()->school_id)
            ->where('term_id', $request->input('term'))
            ->groupBy('subject_id')
            ->with('subject')
            ->with('subject.subjectmain')
            ->select('subject_id')
            ->get();

        return response()->json([
            'subject' => $subject,
            'filter_subject' => $request->input('filter_subject'),
            'row' => count($subject)
        ]);
    }

    public function getClasses(Request $request)
    {
        $classes = Advisor::where('SchTeacher_id',  Auth()->user()->teacher->id)
            ->where('school_id', Auth()->user()->school_id)
            ->where('term_id', $request->input('term'))
            ->groupBy('classes_id')
            ->select('classes_id')
            ->with('classes')
            ->get();

        return response()->json([
            'classes' => $classes,
            'filter_classes' => $request->input('filter_classes'),
            'row' => count($classes)
        ]);
    }

    public function getRoom(Request $request)
    {
        $rooms = Advisor::where('SchTeacher_id',  Auth()->user()->teacher->id)
            ->where('school_id', Auth()->user()->school_id)
            ->where('term_id', $request->input('term'))
            ->where('classes_id', $request->input('classes'))
            ->select('room')
            ->with('classes')
            ->get();

        return response()->json([
            'rooms' => $rooms,
            'filter_room' => $request->input('filter_room'),
            'filter_classes' => $request->input('filter_classes'),
            'row' => count($rooms)
        ]);
    }

    public function download(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit','1024M');

        $filter_term = $request->input('term')==""?null:$request->input('term');
        $filter_subject = $request->input('subject')==""?null:$request->input('subject');
        $filter_classes = $request->input('classes')==""?null:$request->input('classes');
        $payloads = array();

        if ($filter_term != null && $filter_subject != null && $filter_classes != null) {
            $term = Term::where('id', $request->input('term'))
                ->firstorfail();

            $subject  = Subject::where('id', $request->input('subject'))
                ->firstorfail();
            
            $classes = Classes::where('id', $request->input('classes'))
                ->firstorfail();

            $payloads = SubjectLesson::where([
                'subject_lesson.subject_id' => $filter_subject,
                'subject_lesson.classes_id' => $filter_classes,
                'subject_lesson.year'       => $term->year,
            ])->select('subject_lesson.*','subject_lesson_school_order.sort AS school_sort')
            ->leftJoin('subject_lesson_school_order', 'subject_lesson_school_order.lesson_id', '=', 'subject_lesson.id')
            ->where('subject_lesson_school_order.sort', '>', '0')
            ->where('subject_lesson_school_order.school_id', Auth()->user()->school_id)
            ->where('subject_lesson_school_order.term_id', $filter_term)
            ->orderBy('level_id')->orderBy('school_sort')
            ->get();
            
            $school = School::where('id', Auth()->user()->school_id)
                ->first();
            
            // return view('reports.lesson.listlesson', compact([
            //     'payloads',
            //     'school',
            //     'term',
            //     'subject',
            //     'classes'
            // ]));
            $pdf = PDF::loadView('reports.lesson.listlesson', compact([
                'payloads',
                'school',
                'term',
                'subject',
                'classes'
            ]))->setPaper('A4', 'landscape');

            return $pdf->stream('document.pdf');
        } else {
            return redirect()->back();
        }
    }
}
