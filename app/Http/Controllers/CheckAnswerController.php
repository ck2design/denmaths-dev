<?php

namespace App\Http\Controllers;

use Alert;
use App\Models\Advisor;
use App\Models\Classes;
use App\Models\School;
use App\Models\Sheetset;
use App\Models\SheetQuestion;
use App\Models\SubjectLesson;
use App\Models\Subject;
use App\Models\Term;
use Illuminate\Http\Request;
use PDF;

class CheckAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter_term = $request->input('filter_term')==""?null:$request->input('filter_term');
        $filter_subject = $request->input('filter_subject')==""?null:$request->input('filter_subject');
        $filter_classes = $request->input('filter_classes')==""?null:$request->input('filter_classes');
        $payloads = array();

        $terms = Term::where('school_id', Auth()->user()->school_id)
            ->orderBy('year', 'desc')
            ->orderBy('term', 'asc')
            ->get();
        
        if ($filter_term != null && $filter_subject != null && $filter_classes != null) {
            $term = Term::where('id', $request->input('filter_term'))
                ->firstorfail();

            $payloads = SubjectLesson::where([
                'subject_lesson.subject_id' => $filter_subject,
                'subject_lesson.classes_id' => $filter_classes,
                'subject_lesson.year'       => $term->year,
            ])->select('subject_lesson.*','subject_lesson_school_order.sort AS school_sort')
            ->leftJoin('subject_lesson_school_order', 'subject_lesson_school_order.lesson_id', '=', 'subject_lesson.id')
            ->where('subject_lesson_school_order.sort', '>', '0')
            ->where('subject_lesson_school_order.school_id', Auth()->user()->school_id)
            ->where('subject_lesson_school_order.term_id', $filter_term)
            ->orderBy('level_id')->orderBy('school_sort')
            ->get();
        }

        return view('checkanswer.index', compact([
            'terms',
            'filter_term',
            'filter_subject',
            'filter_classes',
            'payloads'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function detail(Request $request)
    {
        $payload = Sheetset::where('id', $request->input('sheetset'))
            ->first();
        
        return view('checkanswer.detail', compact(
            'payload'
        ));
    }
    public function check(Request $request)
    {
        $question 	= SheetQuestion::find($request->input('id'));
		$decode 	= json_decode($question->posts);
		$index 		= $request->input('index');
        $result 	= "";

        //ใช้สำหรับนับจำนวนของ foreach ว่าตอนนี้วนไปแล้วกี่รอบโดยให้ค่าตั้งต้นอยู่ที่ 1ไม่ใช่ 0
		if (!empty($decode)) {
			foreach ($decode as $indexDecode => $valueDecode) {
				++$indexDecode;
				if ( $index == $indexDecode) {
					switch ($valueDecode->type) {
						case 'choice':
							if (! empty($valueDecode->correct)) {
								$correct 	= explode('|', $valueDecode->correct);
								$result 	= in_array($request->input('choose'), $correct) ? true : false;
							} else {
								$request = false;
							}
						break;
							
						case 'choiceimage':
							if (! empty($valueDecode->correct)) {
								$correct 	= explode('|', $valueDecode->correct);
								$result 	= in_array($request->input('choose'), $correct) ? true : false;
							} else {
								$result = false;
							}
						break;
							
						case 'write':
							if (! empty($valueDecode->answer)) {
								if (is_numeric($request->input('choose'))) {
									$choose = $request->input('choose');
								} else {
									$choose = $request->input('choose');
								}
								// $correct 	= explode(',', $valueDecode->answer);
								$result 	= $choose==$valueDecode->answer? true : false;
							} else {
								$result = false;
							}
						break;
							
						case 'choicewrite':
							if (! empty($valueDecode->answerWrite) && ! empty($valueDecode->correctChoice)) {
								// $correctWrite 	= explode(',', $valueDecode->answerWrite);
								$correctChoice 	= explode('|', $valueDecode->correctChoice);
								if (is_numeric($request->input('chooseWrited'))) {
									$choose = $request->input('chooseWrited');
								} else {
									$choose = $request->input('chooseWrited');
								}

								$result = ( $choose==$valueDecode->answerWrite) && (in_array($request->input('chooseChoice'), $correctChoice) ? true : false);
							}
						break;
					}
				}
			}
		}
		
		return response()->json(['status' => true, 'result' => $result ]);
    }

    public function getAnswer(Request $request)
	{	
        $question = SheetQuestion::where('id', $request->input('question'))
            ->first();

		if ($request->input('type')==1) {
			return response()->json([
				'html' => $question->explanation ? $question->explanation . '<br><a class="btn btn-danger removeAnswer">ปิดเฉลย</a>' : null
			]);
		} else {
			$explanation = "";
			$decode = json_decode($question->posts);
			foreach ($decode as $indexDecode => $valueDecode) {
				if ($indexDecode==($request->input('index')-1)) {
					$explanation = empty($valueDecode->explanation) ? "" : $valueDecode->explanation;
					$explanation_type = empty($valueDecode->explanation_type) ? "" : $valueDecode->explanation_type;
				}
			}

			if ($explanation_type==2) {
				$explanation = "<img src='".asset('question/'.$explanation)."' width='300' />";
			} elseif($explanation_type==3) {
				$explanation = "<iframe width='400' height='300' src='https://www.youtube.com/embed/".$explanation."'></iframe>";
			}

			return response()->json([
				'html' => $explanation.'<br><a class="removeAnswerPart">ปิดเฉลย</a>'
			]);
		}
	}
}
