<?php

namespace App\Http\Controllers\profile;

use Alert;
use App\Http\Requests\profile\EmergencyRequest;
use App\Http\Controllers\Controller;
use App\Models\Teacher;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class EmergencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.emergency.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmergencyRequest $request)
    {
        $auth = Auth::user();
        $teacher = Teacher::find($auth->teacher->id);
        if ($teacher) {
            Teacher::where('id', $auth->teacher->id)->update([
                'emergency_name1' => $request->input('emergency_name1'),
                'emergency_name2' => $request->input('emergency_name2'),
                'emergency_name3' => $request->input('emergency_name3'),
                'emergency_relationship1' => $request->input('emergency_relationship1'),
                'emergency_relationship2' => $request->input('emergency_relationship2'),
                'emergency_relationship3' => $request->input('emergency_relationship3'),
            ]);

            Alert::html('บันทึกสำเร็จ', "ทำการอัพเดตข้อมูลติดต่อฉุกเฉินเรียบร้อยแล้ว", 'success');
            return redirect()->back();

        } else {
            Alert::html('ข้อมูลผิดพลาด', "ไม่สามารถอัพเดตข้อมูลข้อมูลติดต่อฉุกเฉินได้", 'error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

