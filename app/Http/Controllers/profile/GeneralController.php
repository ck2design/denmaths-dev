<?php

namespace App\Http\Controllers\profile;

use Alert;
use App\Http\Requests\profile\GeneralRequest;
use App\Http\Controllers\Controller;
use App\Models\Teacher;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public $uploadPath = 'uploads/profiles/teachers';
    public $extension = array('jpg', 'jpeg', 'png');
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('profile.general.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GeneralRequest $request)
    {
        $auth = Auth::user();
        $teacher = Teacher::find($auth->teacher->id);
        if ($teacher) {
            $teacher->update([
                'firstname' => $request->input('firstname'),
                'lastname' => $request->input('lastname'),
                'firstname_en' => $request->input('firstname_en'),
                'lastname_en' => $request->input('lastname_en'),
                'birthday' => $request->input('birthday'),
                'high' => $request->input('high'),
                'weight' => $request->input('weight'),
                'nationality' => $request->input('nationality'),
                'race' => $request->input('race'),
                'phone' => $request->input('phone'),
                'email' => $request->input('email'),
                'status' => $request->input('status')
            ]);

            $user = User::find($auth->id);
            $user->update([
                'active' => $request->input('status')=="W"?0:1,
            ]);

            if ($request->hasFile('picture')) {
                $file = $request->file('picture');
                $fileName = md5(time()) . '.' . $file->getClientOriginalExtension();
                $destination_path = $this->uploadPath;
                if (in_array($file->getClientOriginalExtension(), $this->extension)) {
                    if ($file->move($destination_path, $fileName)) {
                        if (file_exists($auth->teacher->picture)){
                            @unlink($auth->teacher->picture);
                        }
                        Teacher::where('id', $auth->teacher->id)->update([
                            'picture' => $this->uploadPath . '/' . $fileName,
                        ]);
                    }
                }
            }

            Alert::html('บันทึกสำเร็จ', "ทำการอัพเดตข้อมูลส่วนตัวเรียบร้อยแล้ว", 'success');
            return redirect()->back();

        } else {
            Alert::html('ข้อมูลผิดพลาด', "ไม่สามารถอัพเดตข้อมูลส่วนตัวได้", 'error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
