<?php

namespace App\Http\Controllers;

use App\Models\Advisor;
use App\Models\Term;
use App\Models\ScoreExercise;
use App\Models\Sheetset;
use App\Models\StudentHistory;
use App\Models\SubjectLesson;
use Illuminate\Http\Request;

class CheckExerciseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter_term = $request->input('filter_term')==""?null:$request->input('filter_term');
        $filter_subject = $request->input('filter_subject')==""?null:$request->input('filter_subject');
        $filter_classes = $request->input('filter_classes')==""?null:$request->input('filter_classes');
        $filter_room = $request->input('filter_room')==""?null:$request->input('filter_room');
        $payloads = array();
        $exercise = array();

        $terms = Term::where('school_id', Auth()->user()->school_id)
            ->orderBy('year', 'desc')
            ->orderBy('term', 'asc')
            ->get();
        
        if ($filter_term != null && $filter_subject != null && $filter_classes != null && $filter_room != null) {
            $term = Term::where('id', $request->input('filter_term'))
                ->firstorfail();

            $payloads = SubjectLesson::where([
                'subject_lesson.subject_id' => $filter_subject,
                'subject_lesson.classes_id' => $filter_classes,
                'subject_lesson.year'       => $term->year,
            ])->select('subject_lesson.*','subject_lesson_school_order.sort AS school_sort')
            ->leftJoin('subject_lesson_school_order', 'subject_lesson_school_order.lesson_id', '=', 'subject_lesson.id')
            ->where('subject_lesson_school_order.sort', '>', '0')
            ->where('subject_lesson_school_order.school_id', Auth()->user()->school_id)
            ->where('subject_lesson_school_order.term_id', $filter_term)
            ->orderBy('level_id')->orderBy('school_sort')
            ->get();

            $exercise = ScoreExercise::where('term_id', $filter_term)
                ->where('classes_id', $filter_classes)
                ->where('room', $filter_room)
                ->where('status', 1)
                ->get();
        }

        return view('checkexercise.index', compact([
            'terms',
            'filter_term',
            'filter_subject',
            'filter_classes',
            'filter_room',
            'payloads',
            'exercise'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $term = $request->input('term');
        $room = $request->input('room');
        $sheetset = SheetSet::where('id', $request->input('sheetset'))
            ->first();
        if(!$sheetset) {
            return redirect()->back()->withError("ไม่พบแบบฝึกหัดนี้อยู่ในระบบ");
        }

        $historys = StudentHistory::join('SchStudent', 'SchStudent.id', '=', 'schstudent_history.student_id')
                                ->where('subject_id', $request->input('subject_id'))
                                ->where('schstudent_history.term_id', $request->input('term'))
                                ->where('classes_id', $request->input('classes_id'))
                                ->where('room', $request->input('room'))
                                ->where('school_id', auth()->user()->teacher->SchoolId)
                                ->select('schstudent_history.*','student_id','Std_Id', 'Std_Fname', 'Std_Lname', 'Number')
                                ->get();

        return view('checkexercise.create', compact(
            'sheetset',
            'historys',
            'term',
            'room'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
