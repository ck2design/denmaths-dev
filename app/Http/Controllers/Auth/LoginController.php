<?php

namespace App\Http\Controllers\Auth;

use Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Hash;

class LoginController extends Controller
{
    public function index()
    {
        return view('Auth.login');

        if (Auth::check()) {
            $auth = Auth::user();
            return redirect()->route('dashboard.index');
        } else{
            return view('Auth.login');
        }
    }

    public function login(Request $request)
    {
        $user = User::where('username', $request->input('username'))
                    ->where('type', 5)
                    ->where('active', 1)
                    ->first();
        if ($user) {
            if (Hash::check($request->input('password'), $user->password)) {
                if (Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password')], true)) {
                    return redirect()->route('dashboard.index');
                } else {
                    Alert::html('ข้อมูลผิดพลาด', "ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง", 'error');
                }
            } else {
                Alert::html('ข้อมูลผิดพลาด', "ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง", 'error');
            }
        } else {
            Alert::html('ข้อมูลผิดพลาด', "ชื่อผู้ใช้ หรือ รหัสผ่านไม่ถูกต้อง", 'error');
        }

        return redirect()->back();
        // return redirect()->route('dashboard.index');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function username()
    {
        return 'username';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
