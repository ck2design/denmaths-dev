<?php

namespace App\Http\Controllers\student\profile;

use Alert;
use App\Http\Requests\student\profile\GeneralRequest;
use App\Http\Controllers\Controller;
use App\Models\Classes;
use App\Models\Student;
use App\Models\Term;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public $uploadPath = 'student_profile';
    public $extension = array('jpg', 'jpeg', 'png');
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $payload = Student::where('id', $request->input('id'))
            ->where('SchoolId', Auth()->user()->school_id)
            ->firstorfail();
        $classes = Classes::all();
        $terms = Term::where('school_id', Auth()->user()->school_id)
            ->orderBy('year', 'desc')
            ->orderBy('term', 'asc')
            ->get();

        return view('student.profile.general.index', compact(
            'payload',
            'classes',
            'terms'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GeneralRequest $request)
    {
        $payload = Student::where('id', $request->input('id'))
            ->where('SchoolId', Auth()->user()->school_id)
            ->firstorfail();

        if ($payload) {
            $payload->update([
                'BeginClass' => $request->input('begin_class'),
                'BeginDate' => $request->input('begin_date'),
                'Class_ID' => $request->input('classes'),
                'Number' => $request->input('number'),
                'start_system_term_id' => $request->input('term_start'),
                'Std_Bdate' => $request->input('birthday'),
                'StdNationality' => $request->input('nationality'),
                'Std_PersonalID' => $request->input('person_id'),
                'Std_Religious' => $request->input('religious'),
                'StdRace' => $request->input('race'),
                'Std_Status' => $request->input('status'),
                'Std_Sname' => $request->input('sname'),
                'Std_Id' => $request->input('code'),
                'SubClass' => $request->input('room'),
                'term_id' => $request->input('term'),
            ]);

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = $request->input('id'). '.' . $file->getClientOriginalExtension();
                $destination_path = $this->uploadPath;
                if (in_array($file->getClientOriginalExtension(), $this->extension)) {
                    if ($file->move($destination_path, $fileName)) {
                        if (file_exists($payload->Std_image)){
                            @unlink($payload->Std_image);
                        }
                        Student::where('id', $request->input('id'))->update([
                            'Std_image' => $this->uploadPath . '/' . $fileName,
                        ]);
                    }
                }
            }

            Alert::html('บันทึกสำเร็จ', "ทำการอัพเดตข้อมูลส่วนตัวเรียบร้อยแล้ว", 'success');
            return redirect()->back();

        } else {
            Alert::html('ข้อมูลผิดพลาด', "ไม่สามารถอัพเดตข้อมูลส่วนตัวได้", 'error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
