<?php

namespace App\Http\Controllers\student\profile;

use Alert;
use App\Http\Requests\student\profile\GrowthRequest;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class GrowthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $payload = Student::where('id', $request->input('id'))
            ->where('SchoolId', Auth()->user()->school_id)
            ->firstorfail();

        return view('student.profile.growth.index', compact(
            'payload'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GrowthRequest $request)
    {
        $payload = Student::where('id', $request->input('id'))
            ->where('SchoolId', Auth()->user()->school_id)
            ->firstorfail();
        if ($payload) {
            $payload->update([
                'Weight' => $request->input('weight'),
                'Height' => $request->input('height'),
                'GrpBlood' => $request->input('blood'),
                'SpacialAbility' => $request->input('ability')
            ]);

            Alert::html('บันทึกสำเร็จ', "ทำการอัพเดตข้อมูลติดต่อฉุกเฉินเรียบร้อยแล้ว", 'success');
            return redirect()->back();
        } else {
            Alert::html('ข้อมูลผิดพลาด', "ไม่สามารถอัพเดตข้อมูลติดต่อฉุกเฉินได้", 'error');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
