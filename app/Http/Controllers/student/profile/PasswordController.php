<?php

namespace App\Http\Controllers\student\profile;

use Alert;
use App\Http\Requests\profile\PasswordRequest;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\User;
use Hash;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $payload = Student::where('id', $request->input('id'))
            ->where('SchoolId', Auth()->user()->school_id)
            ->firstorfail();

        return view('student.profile.password.index', compact([
            'payload'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payload = Student::where('id', $request->input('id'))
            ->where('SchoolId', Auth()->user()->school_id)
            ->firstorfail();

        if (Hash::check($request->input('password_current'), $payload->PasswordHash)) {
            $payload->update([
                'PasswordHash' => Hash::make($request->input('password')),
                'Password' => $request->input('password')
            ]);

            Alert::html('บันทึกสำเร็จ', "ทำการเปลี่ยนรหัสผ่านของนักเรียนเรียบร้อยแล้ว", 'success');
        } else {
            Alert::html('ข้อมูลผิดพลาด', "รหัสผ่านของนักเรียนไม่ถูก กรุณาใส่รหัสผ่านใหม่", 'error');
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
