<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScoreExercise extends Model // implements AuditableContract
{
    // use SoftDeletes;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    // protected static function boot()
    // {
    //     parent::boot();
    //     static::addGlobalScope(new ProjectScope());
    // }

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'score_exercise';

    /**
     * override Eloquent's default a primary key column named id.
     * @var string
     */
    // protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    // public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;
    // const CREATED_AT = null;
    // const UPDATED_AT = null;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    // protected $appends = [
    //     //
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     //
    // ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = [
    //     //
    // ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    // protected $dispatchesEvents = [
    //     //
    // ];

    /**
     * The attributes that should be encrypted on save.
     *
     * @var array
     */
    // protected $encrypted = [
    //     //
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'lesson_id',
        'sheetset_id',
        'student_id',
        'classes_id',
        'room',
        'term_id',
        'send_date',
        'content',
        'status',
        'redemptions_status',
        'type',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be hashed on save.
     *
     * @var array
     */
    // protected $hashed = [
    //     //
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     //
    // ];

    /**
     * Attribute Getters
     */
    // public function getExampleAttribute()
    // {
    //     return $this->attributes['example'] / 2 ;
    // }

    /**
     * Attribute Setters
     */
    // public function setExampleAttribute($newExample)
    // {
    //     $this->attributes['example'] = $newExample * 2;
    // }

    /**
     * Relationships
     */
    public function lesson()
    {
        return $this->hasOne(SubjectLesson::class, 'id', 'lesson_id');
    }

    public function sheetset()
    {
        return $this->hasOne(Sheetset::class, 'id', 'sheetset_id');
    }

    public function student()
    {
        return $this->hasOne(Student::class, 'id', 'student_id');
    }

    public function term()
    {
        return $this->hasOne(Term::class, 'id', 'term_id');
    }
    /**
     * Scopes
     */
    // public function scopeExample(Builder $query, $q) {
    //     return $query->where('example', '=', $q);
    // }
}
