<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model // implements AuditableContract
{
    // use SoftDeletes;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    // protected static function boot()
    // {
    //     parent::boot();
    //     static::addGlobalScope(new ProjectScope());
    // }

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'schstudent';

    /**
     * override Eloquent's default a primary key column named id.
     * @var string
     */
    // protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    // public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;
    // const CREATED_AT = null;
    // const UPDATED_AT = null;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    // protected $appends = [
    //     //
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     //
    // ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = [
    //     //
    // ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    // protected $dispatchesEvents = [
    //     //
    // ];

    /**
     * The attributes that should be encrypted on save.
     *
     * @var array
     */
    // protected $encrypted = [
    //     //
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'SchoolId',
        't',
        'Std_Id',
        'Class_ID',
        'BeginClass',
        'SubClass',
        'BeginDate',
        'Std_status',
        'Std_Status_Comment',
        'Std_Fname',
        'Std_Lname',
        'Std_FnameEN',
        'Std_LnameEN',
        'Std_Sname',
        'Std_Sex',
        'Std_Prefix',
        'Std_Bdate',
        'Std_Phone',
        'Std_Address',
        'Std_Religious',
        'Template',
        'Monk',
        'TemplateProvince',
        'Hospital',
        'Hos_Tumbol',
        'Hos_Amphur',
        'Hos_Province',
        'Std_PersonalID',
        'OldSchool',
        'SchoolAmphur',
        'SchoolProvince',
        'OldGrade',
        'Terminate',
        'Parent_id',
        'Ids',
        'Std_Stay',
        'TerminateGrp',
        'Weight',
        'Height',
        'GrpBlood',
        'SpacialAbillity',
        'StayWith',
        'StdAddress',
        'StdTumbol',
        'StdAmphur',
        'StdProvince',
        'StdZipcode',
        'StdTel',
        'StdMobile',
        'StdEmail',
        'Ercontract',
        'ErTel',
        'ErRemark',
        'EduProgramID',
        'EduPP1',
        'EduCertificateNo',
        'EduDateEnd',
        'EduDateTerminate',
        'EduRemarkTerminate',
        'OldSchProgramId',
        'OldSchClassLevel',
        'SchoolDateIn',
        'StdTmp',
        'Username',
        'Password',
        'PasswordHash',
        'ClearTerm',
        'Std_Stay_New',
        'Std_Stat',
        'StdNationality',
        'StdRace',
        'Std_image',
        'Number',
        'admissionno',
        'Parent1_fullname',
        'Parent1_phone',
        'Parent1_relations',
        'Parent2_fullname',
        'Parent2_phone',
        'Parent2_relations',
        'Std_Score',
        'start_system_term_id',
        'term_id',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hashed on save.
     *
     * @var array
     */
    // protected $hashed = [
    //     //
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     //
    // ];

    /**
     * Attribute Getters
     */
    // public function getExampleAttribute()
    // {
    //     return $this->attributes['example'] / 2 ;
    // }

    /**
     * Attribute Setters
     */
    // public function setExampleAttribute($newExample)
    // {
    //     $this->attributes['example'] = $newExample * 2;
    // }

    /**
     * Relationships
     */
    public function term()
    {
        return $this->hasOne(Term::class, 'id', 'term_id');
    }

    public function classes()
    {
        return $this->hasOne(Classes::class, 'id', 'Class_ID');
    }

    public function subjects()
    {
        return $this->hasMany(StudentSubject::class, 'student_id', 'id');
    }

    public function Historys()
    {
        return $this->hasMany(StudentHistory::class, 'student_id', 'id');
    }

    public function school()
    {
        return $this->hasOne(School::class, 'id', 'SchoolId');
    }

    /**
     * Scopes
     */
    // public function scopeExample(Builder $query, $q) {
    //     return $query->where('example', '=', $q);
    // }
}
