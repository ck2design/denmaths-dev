<?php

namespace App\Models;

use App\Models\Sheetset;
use Illuminate\Database\Eloquent\Model;

class SubjectLesson extends Model
{
    // use SoftDeletes;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    // protected static function boot()
    // {
    //     parent::boot();
    //     static::addGlobalScope(new ProjectScope());
    // }

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subject_lesson';

    /**
     * override Eloquent's default a primary key column named id.
     * @var string
     */
    // protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    // public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;
    // const CREATED_AT = null;
    // const UPDATED_AT = null;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    // protected $appends = [
    //     //
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     //
    // ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = [
    //     //
    // ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    // protected $dispatchesEvents = [
    //     //
    // ];

    /**
     * The attributes that should be encrypted on save.
     *
     * @var array
     */
    // protected $encrypted = [
    //     //
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
		'code',
		'year',
		'name',
		'subject_id',
		'classes_id',
		'level_id',
		'sort',
        'sheet_sets_id',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be hashed on save.
     *
     * @var array
     */
    // protected $hashed = [
    //     //
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     //
    // ];

    /**
     * Attribute Getters
     */
    // public function getExampleAttribute()
    // {
    //     return $this->attributes['example'] / 2 ;
    // }

    /**
     * Attribute Setters
     */
    // public function setExampleAttribute($newExample)
    // {
    //     $this->attributes['example'] = $newExample * 2;
    // }

    /**
     * Relationships
     */
    // public function example()
    // {
    //     return $this->hasOne(example::class);
    // }
    public function subject()
    {
        return $this->hasOne(Subject::class, 'id', 'subject_id');
    }

    public function classes()
    {
        return $this->hasOne(Classes::class, 'id', 'classes_id');
    }

    public function sheetsets()
    {
        $sheetsets_explode = explode(",", $this->sheet_sets_id);
        
        $sheetset = Sheetset::whereIn('id', $sheetsets_explode)
            ->get();
        
        return $sheetset;
    }

    public function lablesson($sheetset)
    {
        $lablesson = LabLesson::join('lab', 'lab.id', 'lab_lesson.lab_id')
            ->where('year', $this->year)
            ->where('subject_id', $this->subject_id)
            ->where('lesson_id', $this->id)
            ->where('sheet_sets_id', $sheetset)
            ->orderBy('sheet_sets_id')
            ->orderBy('level_id')
            ->with('sheetset')
            ->first();

        return $lablesson;
    }

	/**
     * Scopes
     */
    // public function scopeExample(Builder $query, $q) {
    //     return $query->where('example', '=', $q);
    // }
}