<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    // use SoftDeletes;
    protected $table = 'users';

    protected $fillable = [
        'id',
        'firstname',
        'lastname',
        'username',
        'password',
        'passwordDisplay',
        'email',
        'school_id',
        'type',
        'reference_id',
        'academic_sign',
        'active',
        'rememver_token',
        'created_at',
        'updated_at',
        'session_id'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'id' => 'string',
    ];

    public function teacher()
    {
        return $this->hasOne(Teacher::class, 'id', 'reference_id');
    }
}
