<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentTerm extends Model // implements AuditableContract
{
    // use SoftDeletes;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    // protected static function boot()
    // {
    //     parent::boot();
    //     static::addGlobalScope(new ProjectScope());
    // }

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fact_student_terms';

    /**
     * override Eloquent's default a primary key column named id.
     * @var string
     */
    // protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    // public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;
    // const CREATED_AT = null;
    // const UPDATED_AT = null;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    // protected $appends = [
    //     //
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     //
    // ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = [
    //     //
    // ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    // protected $dispatchesEvents = [
    //     //
    // ];

    /**
     * The attributes that should be encrypted on save.
     *
     * @var array
     */
    // protected $encrypted = [
    //     //
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'student_id',
        'code',
        'classes_id',
        'room',
        'number',
        'year',
        'term_number',
        'term_id',
        'subject_id',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be hashed on save.
     *
     * @var array
     */
    // protected $hashed = [
    //     //
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     //
    // ];

    /**
     * Attribute Getters
     */
    // public function getExampleAttribute()
    // {
    //     return $this->attributes['example'] / 2 ;
    // }

    /**
     * Attribute Setters
     */
    // public function setExampleAttribute($newExample)
    // {
    //     $this->attributes['example'] = $newExample * 2;
    // }

    /**
     * Relationships
     */
    // public function example()
    // {
    //     return $this->hasOne(example::class);
    // }

    /**
     * Scopes
     */
    // public function scopeExample(Builder $query, $q) {
    //     return $query->where('example', '=', $q);
    // }
}
