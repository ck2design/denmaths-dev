<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Model // implements AuditableContract
{
    // use SoftDeletes;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    // protected static function boot()
    // {
    //     parent::boot();
    //     static::addGlobalScope(new ProjectScope());
    // }

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dim_teachers';

    /**
     * override Eloquent's default a primary key column named id.
     * @var string
     */
    // protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    // public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    // public $timestamps = false;
    // const CREATED_AT = null;
    // const UPDATED_AT = null;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    // protected $appends = [
    //     //
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     //
    // ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = [
    //     //
    // ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    // protected $dispatchesEvents = [
    //     //
    // ];

    /**
     * The attributes that should be encrypted on save.
     *
     * @var array
     */
    // protected $encrypted = [
    //     //
    // ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'Tea_Id',
        'SchoolId',
        'newCode',
        'OldCode',
        'NumberR8',
        'Tea_Fname',
        'Tea_Name',
        'Tea_SName',
        'FnameEng',
        'LnameEng',
        'Tea_Position',
        'BirthDay',
        'Age',
        'Domicile',
        'High',
        'Weigh',
        'Sex',
        'Nationality',
        'Race',
        'Religion',
        'Status',
        'FatherFname',
        'FatherLname',
        'MotherFname',
        'MotherLname',
        'FanFname',
        'FanLname',
        'Trade',
        'IDcard',
        'Vatcard',
        'DriveCard',
        'CarRegis',
        'Village',
        'HomeNumber',
        'Moo',
        'Road',
        'Tumbon',
        'Ampher',
        'Province',
        'ZipCode',
        'Phone',
        'NVillage',
        'NHomeNumber',
        'NMoo',
        'NRoad',
        'NTumbon',
        'NAmpher',
        'NProvince',
        'NZipCode',
        'NPhone',
        'StartWork',
        'Inwork',
        'BankNumber',
        'ChpKNo',
        'ChpSNo',
        'Department',
        'Picture',
        'Prefix',
        'Username',
        'Password',
        'PasswordHash',
        'Portfolio',
        'WorkingStatus',
        'Email',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hashed on save.
     *
     * @var array
     */
    // protected $hashed = [
    //     //
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     //
    // ];

    /**
     * Attribute Getters
     */
    // public function getExampleAttribute()
    // {
    //     return $this->attributes['example'] / 2 ;
    // }

    /**
     * Attribute Setters
     */
    // public function setExampleAttribute($newExample)
    // {
    //     $this->attributes['example'] = $newExample * 2;
    // }

    /**
     * Relationships
     */
    public function school()
    {
        return $this->hasOne(school::class, 'id', 'school_id');
    }

    /**
     * Scopes
     */
    // public function scopeExample(Builder $query, $q) {
    //     return $query->where('example', '=', $q);
    // }
}
